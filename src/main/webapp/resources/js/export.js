function exportChartInPDF(chart) {
    var producto, departamento, municipio, texto;
    var fecha = new Date();
    if (arguments[1] != undefined && arguments[1] != '') {
        producto = arguments[1];
    }

    if (arguments[2] != undefined && arguments[2] != '') {
        departamento = arguments[2];
    }

    if (arguments[3] != undefined && arguments[3] != '') {
        municipio = arguments[3];
    }

    if (departamento == '-1' && municipio == '-1') {
        texto = 'ubicadas en todo el país de El Salvador';
    } else if (departamento != '-1' && municipio == '-1') {
        texto = 'ubicadas en el departamento de ' + departamento;
    } else if (departamento != '-1' && municipio != '-1') {
        texto = 'ubicadas en el municipio de ' + municipio + ' del departamento de ' + departamento;
    }
    var docDefinition = {
        header: {
            columns: [
                {text: 'Utils.do | Tu mejor aliado en la comparativa de precios de útiles escolares', margin: 15, color: 'gray'}
            ]
        },

        footer: {
            columns: [
                {text: 'Utils.do', margin: 15, color: 'gray'}
            ]
        },

        content: [
            {text: '| Gráfico de barras de comparativa de precios del producto (' + producto + ') en distintas librerías', style: 'header'},
            {text: 'Descripción:', style: 'tituloDes'},
            {text: 'El presente gráfico detalla las diferencias de precios que existen para el producto (' + producto + ') en las distintas librerías ' + texto + '.', style: 'parrafo'},
            {
                image: $(PF(chart).exportAsImage()).attr('src'),
                width: 500,
                height: 375
            },
            {text: 'Fecha de generación: ' + fecha.getDate() + '/' + (fecha.getMonth() + 1) + '/' + fecha.getFullYear(), style: 'fecha'}
        ],

        styles: {
            header: {
                fontSize: 18,
                bold: true,
                alignment: 'center',
                color: 'darkgray',
                margin: [8, 17, 8, 17]
            },

            parrafo: {
                fontSize: 12,
                alignment: 'justify',
                color: 'darkgray',
                margin: [8, 10, 8, 30]
            },

            tituloDes: {
                fontSize: 15,
                bold: true,
                alignment: 'left',
                color: 'darkgray',
                margin: [8, 10, 8, 8]
            },

            fecha: {
                fontSize: 12,
                bold: true,
                color: 'darkgray',
                margin: [8, 30, 8, 10]
            }
        }
    };
    pdfMake.createPdf(docDefinition).open();
    pdfMake.createPdf(docDefinition).download('utils.do-grafico.pdf');
}

function getWidgetVarById(id) {
    for (var propertyName in PrimeFaces.widgets) {
        if (PrimeFaces.widgets[propertyName].id === id) {
            return PrimeFaces.widgets[propertyName].widgetVar;
        }
    }
}


