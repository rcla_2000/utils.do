/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.SondeosEntity;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ernesto
 */
@Stateless
public class SondeosModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<SondeosEntity> listarSondeo() {
        Query query = em.createNamedQuery("SondeosEntity.findAll").setMaxResults(25);
        //Query query = em.createNamedQuery("SondeosEntity.likeProducto").setParameter("producto", "Cuaderno");
        //Query query = em.createNamedQuery("SondeoEntity.findByDepartamento").setParameter("departamento", "San Salvador").setMaxResults(25);
        return query.getResultList();
    }

    public List<SondeosEntity> listarPorDepartamento(String departamento) {
        Query query = em.createNamedQuery("SondeosEntity.findByDepartamento").setParameter("departamento", departamento).setMaxResults(25);
        return query.getResultList();
    }
    
    public List<SondeosEntity> listarPorMunicipio(String municipio){
        Query query = em.createNamedQuery("SondeosEntity.findByMunicipio").setParameter("municipio", municipio).setMaxResults(25);
        return query.getResultList();
    }


    public List<SondeosEntity> listarporNombre(String producto) {
        System.out.println("Usted escribio el producto: " + producto);
        Query query = em.createNamedQuery("SondeosEntity.likeProducto").setParameter("producto", producto).setMaxResults(25);
        return query.getResultList();
    }
    
        public List<SondeosEntity> listarporEstablecimiento(String establecimiento) {
        System.out.println("Usted escribio el establecimiento: " + establecimiento);
        //Query query = em.createNamedQuery("SondeosEntity.likeEstablecimiento").setParameter("establecimiento", establecimiento).setMaxResults(25);
        Query query = em.createNamedQuery("SondeosEntity.findByEstablecimiento").setParameter("establecimiento", establecimiento);
        return query.getResultList();
    }
        
        public List<SondeosEntity> listarporEstablecimiento_Producto(String establecimiento, String producto) {
        System.out.println("Usted escribio el establecimiento: " + establecimiento + "y el producto: " + producto);
        //Query query = em.createNamedQuery("SondeosEntity.likeEstablecimiento").setParameter("establecimiento", establecimiento).setMaxResults(25);
        Query query = em.createNamedQuery("SondeosEntity.likeProducto_Establecimiento").setParameter("producto", producto).setParameter("establecimiento", establecimiento);
        return query.getResultList();
    }
        
        public List<SondeosEntity> listarPorId(int idSondeo){
        Query query = em.createNamedQuery("SondeosEntity.findByIdSondeo").setParameter("idSondeo", idSondeo);

        return query.getResultList();
    }

       
    public SondeosEntity obtenerSondeo(int id) {
        return em.find(SondeosEntity.class, id);
    }

    
    public List<SondeosEntity> listaMunicipios(){
        
        Query query = em.createNamedQuery("SondeosEntity.distinctMunicipio");
        
        return query.getResultList();
    }
    
        public List<SondeosEntity> listaMunicipios(String departamento){
        
        Query query = em.createNamedQuery("SondeosEntity.distinctMunicipios").setParameter("departamento", departamento).setMaxResults(25);
        
        return query.getResultList();
    }
    
        public List<SondeosEntity> listaDepartamentos(){
    
        Query query = em.createNamedQuery("SondeosEntity.distinctDepartamento");
        
        return query.getResultList();
    }
        
        public List<SondeosEntity> comparacionProductos(String producto) {
        Query query = em.createNativeQuery("SELECT * FROM sondeos WHERE producto LIKE '%" + producto + "%' GROUP BY establecimiento "
                + "ORDER BY precio, fecha_sondeo", SondeosEntity.class);
        List<SondeosEntity> lista = query.setMaxResults(10).getResultList();
        return lista;
    }

}
