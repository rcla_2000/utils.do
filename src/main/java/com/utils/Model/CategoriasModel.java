/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.CategoriasEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class CategoriasModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<CategoriasEntity> listarCategorias() {
        Query query = em.createNamedQuery("CategoriasEntity.findAll");
        return query.getResultList();
    }

    public int modificarCategoria(CategoriasEntity categoria) {
        try {
            em.merge(categoria);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarCategoria(int id) {

        try {
            CategoriasEntity categoria = em.find(CategoriasEntity.class, id);

            if (categoria != null) {
                em.remove(categoria);
                em.flush();
                return 1;
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int agregarCategoria(CategoriasEntity categoria) {
        try {
            em.persist(categoria);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public CategoriasEntity obtenerCategoria(int id) {
        return em.find(CategoriasEntity.class, id);
    }

}
