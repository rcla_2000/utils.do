/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.ImagenesProductoEntity;
import com.utils.Entities.ProductosEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HP
 */
@Stateless
public class ImagenesModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public int modificarImagen(ImagenesProductoEntity imagen) {
        try {
            em.merge(imagen);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarImagen(int id) {

        try {
            ImagenesProductoEntity imagen = em.find(ImagenesProductoEntity.class, id);

            if (imagen != null) {
                em.remove(imagen);
                em.flush();
                return 1;
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int agregarImagen(ImagenesProductoEntity imagen) {
        try {
            em.persist(imagen);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public ImagenesProductoEntity obtenerImagen(int id) {
        return em.find(ImagenesProductoEntity.class, id);
    }
    
    public ImagenesProductoEntity obtenerImagenProducto(ProductosEntity producto){
        Query query = em.createNamedQuery("ImagenesProductoEntity.findByProducto");
        query.setParameter("producto", producto);
        return (ImagenesProductoEntity) query.getSingleResult();
    }
}
