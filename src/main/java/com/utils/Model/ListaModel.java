/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.ListasEntity;
import com.utils.Entities.UsuariosEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jonat
 */
@Stateless
public class ListaModel {

     @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<ListasEntity> listarListas(UsuariosEntity user){
        Query query = em.createNamedQuery("ListasEntity.findByIdUsuario").setParameter("codigo", user);
        return query.getResultList();
    }
    
    public int agregarLista(ListasEntity lista){
        try{
            em.persist(lista);
            em.flush();
            return 1;
        }catch(Exception e){
            return 0;
        }
    }
    
    public int eliminarLista(String codigo){
        try{
            ListasEntity lista = em.find(ListasEntity.class, codigo);
            if(lista != null){
                em.remove(lista);
                em.flush();
                return 1;
            }
            return 1;
        }catch(Exception e){
            return 0;
        }
    }
    
    public int modificarLista(ListasEntity lista){
        try{
            em.merge(lista);
            em.flush();
            return 1;
        }catch(Exception e){
            return 0;
        }
    }
    
     public ListasEntity buscarLista(String codigo){
        return em.find(ListasEntity.class, codigo);
    }
    
}
