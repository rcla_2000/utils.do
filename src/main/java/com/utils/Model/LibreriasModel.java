/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.LibreriasEntity;
import com.utils.Utils.CorreoMail;
import com.utils.Utils.Utils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Henry
 */
@Stateless
public class LibreriasModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<LibreriasEntity> listarLibrerias() {
        Query query = em.createNamedQuery("LibreriasEntity.findAll");
        return query.getResultList();
    }
    
    public List<String> listarDepartamentos(){
        Query query = em.createNamedQuery("LibreriasEntity.findDepartamentos");
        return query.getResultList();
    }
    
    public List<String> listarMunicipios(String departamento){
        Query query = em.createNamedQuery("LibreriasEntity.findMunicipios");
        query.setParameter("departamento", departamento);
        return query.getResultList();
    }
    
    public List<LibreriasEntity> listarLibreriasPorDepartamento(String departamento){
       Query query = em.createNamedQuery("LibreriasEntity.findByDepartamento");
       query.setParameter("departamento", departamento);
       return query.getResultList();
    }
    
    public List<LibreriasEntity> listarLibreriasPorMunicipio(String municipio){
        Query query = em.createNamedQuery("LibreriasEntity.findByMunicipio");
        query.setParameter("municipio", municipio);
        return query.getResultList();
    }
    
    public List<LibreriasEntity> listarLibreriasPorNombre(String nombre){
        Query query = em.createNamedQuery("LibreriasEntity.findByNombre");
        query.setParameter("nombre", nombre);
        return query.getResultList();
    }
 
    public int agregarLibreria(LibreriasEntity libreria) {
        try {
            em.persist(libreria);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int modificarLibreria(LibreriasEntity libreria) {
        try {
            em.merge(libreria);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarLibreria(String codigo) {

        try {

            LibreriasEntity libreria = em.find(LibreriasEntity.class, codigo);

            if (libreria != null) {
                em.remove(libreria);
                em.flush();
                return 1;
            }

            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public LibreriasEntity obtenerLibreria(String codigo) {
        return em.find(LibreriasEntity.class, codigo);
    }

}
