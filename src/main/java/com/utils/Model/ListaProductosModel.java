/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.ListaProductosEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author jonat
 */
@Stateless
public class ListaProductosModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<ListaProductosEntity> listarListas(String codigo){
        Query query = em.createNativeQuery("SELECT * FROM lista_productos INNER JOIN productos on lista_productos.id_producto = productos.id_producto WHERE lista_productos.id_lista = '" + codigo + "'", ListaProductosEntity.class);
        return query.getResultList();
    }
    
    public int eliminarProducto(int codigo){
        try{
            
            ListaProductosEntity producto = em.find(ListaProductosEntity.class, codigo);
            
            if(producto != null){
                em.remove(producto);
                em.flush();
                return 1;
            }
          
            return 0;
        }catch(Exception e){
            return 0;
        }
    }
    
    public int agregarLista(ListaProductosEntity lista){
        try{
            em.persist(lista);
            em.flush();
            return 1;
        }catch(Exception e){
            return 0;
        }
    }
}
