/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.SolicitudesEntity;
import com.utils.Entities.LibreriasEntity;
import com.utils.Entities.UsuariosEntity;
import com.utils.Entities.TipoUsuariosEntity;
import com.utils.Utils.CorreoMail;
import com.utils.Utils.Utils;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Henry
 */
@Stateless
public class SolicitudesModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<SolicitudesEntity> listarOfertas() {
        Query query = em.createNamedQuery("SolicitudesEntity.findAll");
        return query.getResultList();
    }

    public int rechazarSolicitud(String codigo) {
        try {
            SolicitudesEntity solicitud = em.find(SolicitudesEntity.class, codigo);
            if (solicitud != null) {
                em.remove(solicitud);
                em.flush();
                return 1;
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int aprobarSolicitud(String codigo) {
        try {
            SolicitudesEntity solicitud = em.find(SolicitudesEntity.class, codigo);
            if (solicitud != null) {
                UsuariosEntity usuario = em.find(UsuariosEntity.class, solicitud.getUsuario().getIdUsuario());
                if (usuario != null) {
                    Utils utilidades = new Utils();
                    //Agregar los datos a la libreria
                    LibreriasEntity libreria = new LibreriasEntity();
                    libreria.setIdLibreria(utilidades.generarCodigo());
                    libreria.setNombre(solicitud.getLibreria());
                    libreria.setDireccion(solicitud.getDireccionLibreria());
                    libreria.setDepartamento(solicitud.getDepartamento());
                    libreria.setMunicipio(solicitud.getMunicipio());
                    libreria.setTelefono(solicitud.getTelefono());
                    libreria.setEmail(solicitud.getEmail());
                    libreria.setAdministrador(usuario);
                    em.persist(libreria);
                    em.flush();
                    //Cambiamos el rol de quien envio la solicitud
                    TipoUsuariosEntity tipo = new TipoUsuariosEntity();
                    tipo.setId(2);
                    usuario.setTipoUsuario(tipo);
                    //Removemos la solicitud
                    em.remove(solicitud);
                    em.flush();
                    return 1;
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int agregarSolicitud(SolicitudesEntity solicitud) {
        try {
            em.persist(solicitud);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public SolicitudesEntity obtenerSolicitud(String id) {
        return em.find(SolicitudesEntity.class, id);
    }

}
