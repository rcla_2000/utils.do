/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Beans.LoginBean;
import com.utils.Entities.CategoriasEntity;
import com.utils.Entities.HistorialEntity;
import com.utils.Entities.LibreriaProductoEntity;
import com.utils.Entities.LibreriasEntity;
import com.utils.Entities.ProductosEntity;
import com.utils.Entities.UsuariosEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import com.utils.Model.HistorialModel;
import java.util.Date;

/**
 *
 * @author HP
 */
@Stateless
public class ProductosModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;
    
    private HistorialModel historialmodel = new HistorialModel();
    private HistorialEntity historial = new HistorialEntity();

    public List<ProductosEntity> listarProductos() {
        Query query = em.createNamedQuery("ProductosEntity.findAll");
        return query.getResultList();
    }

    public List<ProductosEntity> listarProductosPorNombre(String nombre) {
        Query query = em.createNamedQuery("ProductosEntity.findByNombre");
        query.setParameter("nombre", nombre);
        return query.getResultList();
    }

    public List<ProductosEntity> listarProductosPorLibreria(String idLibreria) {
        Query query = em.createNativeQuery("SELECT p.* from productos p INNER JOIN libreria_producto lp on p.id_producto = lp.id_producto INNER JOIN librerias l on lp.id_libreria = l.id_libreria\n"
                + "where l.id_libreria = '" + idLibreria + "'", ProductosEntity.class);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarProductosPorAdministrador(UsuariosEntity administrador) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.productosPorAdministrador");
        query.setParameter("administrador", administrador);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarPreciosProducto(ProductosEntity producto) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.findByProducto");
        query.setParameter("producto", producto);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarPreciosProductoPorDepartamento(ProductosEntity producto, String departamento) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.findByDepartamento");
        query.setParameter("producto", producto);
        query.setParameter("departamento", departamento);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarPreciosProductoPorMunicipio(ProductosEntity producto, String municipio) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.findByMunicipio");
        query.setParameter("producto", producto);
        query.setParameter("municipio", municipio);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarPreciosMayores(ProductosEntity producto) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.findByMaxPrecio");
        query.setParameter("producto", producto);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarPreciosMenores(ProductosEntity producto) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.findByMinPrecio");
        query.setParameter("producto", producto);
        return query.getResultList();
    }

    public List<LibreriaProductoEntity> listarComparacionPersonalizada(ProductosEntity producto, List<LibreriasEntity> librerias) {
        Query query = em.createNamedQuery("LibreriaProductoEntity.comparacionPrecio");
        query.setParameter("producto", producto);
        query.setParameter("librerias", librerias);
        return query.getResultList();
    }

    public List<ProductosEntity> listarProductosPorCategoria(CategoriasEntity categoria) {
        Query query = em.createNamedQuery("ProductosEntity.findByCategoria");
        query.setParameter("idCategoria", categoria);
        return query.getResultList();
    }

    public List<ProductosEntity> listaProductosSinImagen() {
        Query query = em.createNativeQuery("SELECT p.* from productos p left join imagenes_producto i on p.id_producto = i.id_producto where i.id_producto is null", ProductosEntity.class);
        return query.getResultList();
    }

    public int modificarProducto(ProductosEntity producto) {
        try {
            em.merge(producto);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int modificarProductoLibreria(LibreriaProductoEntity lp) {
        try {
            em.merge(lp);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarProducto(String id) {

        try {
            ProductosEntity p = em.find(ProductosEntity.class, id);
            if (p != null) {
                em.remove(p);
                em.flush();
                return 1;
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarProductoLibreria(String id) {

        try {
            LibreriaProductoEntity lp = em.find(LibreriaProductoEntity.class, id);
            if (lp != null) {
                em.remove(lp);
                em.flush();
                return 1;
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int agregarProducto(ProductosEntity producto) {
        try {
            em.persist(producto);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int agregarProductoLibreria(LibreriaProductoEntity lp) {
        try {
            em.persist(lp);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public ProductosEntity obtenerProducto(String id) {
        return em.find(ProductosEntity.class, id);
    }

    public LibreriaProductoEntity obtenerProductoLibreria(int id) {
        return em.find(LibreriaProductoEntity.class, id);
    }
}
