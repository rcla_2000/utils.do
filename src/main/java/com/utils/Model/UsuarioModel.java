/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Entities.UsuariosEntity;
import com.utils.Utils.CorreoMail;
import com.utils.Utils.Utils;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Jonathan
 */
@Stateless
public class UsuarioModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;

    public List<UsuariosEntity> listarUsuarios() {
        Query query = em.createNamedQuery("UsuariosEntity.findAll");
        return query.getResultList();
    }

    public int modificarUsuario(UsuariosEntity usuario) {
        try {
            em.merge(usuario);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarUsuario(String codigo) {

        try {

            UsuariosEntity usuario = em.find(UsuariosEntity.class, codigo);

            if (usuario != null) {
                em.remove(usuario);
                em.flush();
                return 1;
            }

            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public int agregarUsuario(UsuariosEntity usuario) {
        try {
            em.persist(usuario);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int modificarPass(String correo) {
        try {
            Utils utilidades = new Utils();
            String password = utilidades.generatePassword();
            //Convertimos a md5
            String pass = Utils.getMD5(password);
            String jpql = "UPDATE UsuariosEntity u "
                    + "SET u.password = :password "
                    + "WHERE u.email = :email";
            em.createQuery(jpql)
                    .setParameter("password", pass)
                    .setParameter("email", correo)
                    .executeUpdate();

            CorreoMail.CorreoRecovery(correo, password);

            return 1;
        } catch (Exception e) {
            return 0;
        }

    }
    
    public List<Long> obtenerEdadesUsuarios(){
        String sql = "SELECT YEAR(NOW()) - YEAR(fecha_nacimiento) from usuarios";
        Query query = em.createNativeQuery(sql);
        List<Long> edades = query.getResultList();
        return edades;
    }

    public UsuariosEntity buscarUsuario(String codigo) {
        return em.find(UsuariosEntity.class, codigo);
    }
    
    public int cantidadUsuariosGenero(Character genero){
        Query query = em.createNamedQuery("UsuariosEntity.findBySexo");
        query.setParameter("sexo", genero);
        return query.getResultList().size();
    }

}
