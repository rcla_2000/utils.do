/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Model;

import com.utils.Beans.LoginBean;
import com.utils.Entities.HistorialEntity;
import com.utils.Entities.UsuariosEntity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Ernesto
 */
@Stateless
public class HistorialModel {

    @PersistenceContext(unitName = "Persistence")
    private EntityManager em;
    
    public static String busquedaporNombre = "";
    public static String busquedaporCategoria = "";
    public static String busquedaporDepartamento = "";
    public static String busquedaporMunicipio = "";
    
    public int agregarAlHistorial(HistorialEntity historial) {
        try {
            em.persist(historial);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }
    
    public int modificarHistorial(HistorialEntity historial) {
        try {
            em.merge(historial);
            em.flush();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int eliminarLibreria(String codigo) {

        try {

            HistorialEntity historial = em.find(HistorialEntity.class, codigo);

            if (historial != null) {
                em.remove(historial);
                em.flush();
                return 1;
            }

            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public HistorialEntity obtenerLibreria(String codigo) {
        return em.find(HistorialEntity.class, codigo);
    }

   public List<HistorialEntity> listarHistorialporUsuario(UsuariosEntity idUsuario){
        Query query = em.createNamedQuery("HistorialEntity.findByIdUsuario");
        query.setParameter("idUsuario", idUsuario);
        return query.getResultList();
    }
   

   public List<HistorialEntity> listarHistorialporNombre(String busqueda, UsuariosEntity idUsuario){
        Query query = em.createNamedQuery("HistorialEntity.findByBusqueda");
        query.setParameter("busqueda", busqueda);
        query.setParameter("idUsuario", idUsuario);
        return query.getResultList();
    }
   
   public List<HistorialEntity> listarHistorialporFecha(String fecha, String idUsuario){
        System.out.print("fecha formateada: -> " + fecha + "\nUsuario: " + idUsuario);
        Query query = em.createNativeQuery("SELECT * FROM `historial` WHERE fecha_hora like '%" + fecha +
                "%' and id_usuario = '" + idUsuario +"'",HistorialEntity.class);
        
        System.out.print("-->" + query.toString());
        return query.getResultList();
    }

    
}
