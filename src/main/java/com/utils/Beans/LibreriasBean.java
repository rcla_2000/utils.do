/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.LibreriasEntity;
import com.utils.Model.LibreriasModel;
import com.utils.Utils.jsfUtil;
import com.utils.Utils.validation;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import com.utils.Utils.Utils;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.PostConstruct;

/**
 *
 * @author Henry
 */
@Named(value = "libreriasBean")
@SessionScoped
public class LibreriasBean implements Serializable {

    @EJB
    private LibreriasModel libreriasModel;
    private LibreriasEntity libreria = new LibreriasEntity();
    List<LibreriasEntity> listaLibrerias;
    List<LibreriasEntity> listaFiltradaLibrerias;
    private List<String> listaDepartamentos;
    private List<String> listaMunicipios;

    public LibreriasBean() {
    }

    @PostConstruct
    public void init() {
        listaLibrerias = libreriasModel.listarLibrerias();
        listaMunicipios = new ArrayList<>();
    }

    public void listarLibreriasPorDepartamento() {
        if (libreria.getDepartamento().equals("-1")) {
            listaLibrerias = libreriasModel.listarLibrerias();
        } else {
           
            listaLibrerias = libreriasModel.listarLibreriasPorDepartamento(libreria.getDepartamento());
            
        }
    }
    
    public void listarLibreriasPorMunicipio() {
        if (libreria.getMunicipio().equals("-1")) {
            listaLibrerias = libreriasModel.listarLibreriasPorDepartamento(libreria.getDepartamento());
        } else {
            
            listaLibrerias = libreriasModel.listarLibreriasPorMunicipio(libreria.getMunicipio());
        }
    }
    
    public void listarLibreriasPorNombre(){
        if(libreria.getNombre().trim().equals("")){
            listaLibrerias = libreriasModel.listarLibrerias();
        }else{
            
            listaLibrerias = libreriasModel.listarLibreriasPorNombre(libreria.getNombre());
        }
        
    }
    
    public void seleccionarLibreria(String codigo) throws IOException{
        this.libreria = libreriasModel.obtenerLibreria(codigo);
        FacesContext.getCurrentInstance().getExternalContext().redirect("detallesLibreria.xhtml"); 
    }
    
    public void seleccionarLibreriaAdministrador(String codigo) throws IOException{
        this.libreria = libreriasModel.obtenerLibreria(codigo);
        FacesContext.getCurrentInstance().getExternalContext().redirect("../librerias/detalles.xhtml"); 
    }
    
    public List<LibreriasEntity> getListaLibrerias() {
        return listaLibrerias;
    }

    public List<String> getListaDepartamentos() {
        listaDepartamentos = libreriasModel.listarDepartamentos();
        return listaDepartamentos;
    }
    
    public void setListaMunicipios(String departamento){
        listaMunicipios = libreriasModel.listarMunicipios(departamento);
    }
    
    public List<String> getListaMunicipios(){
        return listaMunicipios;
    }

    public LibreriasEntity getLibreria() {
        return libreria;
    }

    public void setLibreria(LibreriasEntity libreria) {
        this.libreria = libreria;
    }

    public void agregarLibreria() {
        libreria.setIdLibreria(Utils.generarCodigo());
        libreria.setTelefono("-");
        libreria.setEmail("-");
        if (libreriasModel.agregarLibreria(libreria) > 0) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success", "Libreria agregada exitossamente");
            libreria = new LibreriasEntity();
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Ocurrio un error al momento de agregar la libreria");
        }
    }

    public String modificarLibreria() {
        if (libreriasModel.modificarLibreria(libreria) > 0) {
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registrado exitosamente!"));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success", "Libreria modificada exitossamente");
            libreria = new LibreriasEntity();
            return "/librerias/GestionarLibrerias?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Ocurrio un error a la hora de modificar");
            return null;
        }
    }

    public String obtenerLibreria(String codigo) {
        libreria = libreriasModel.obtenerLibreria(codigo);
        return "/librerias/ModificarLibreria?faces-redirect=true";
    }

    public String eliminarLibreria(String codigo) {

        if (libreriasModel.eliminarLibreria(codigo) > 0) {
            //Mensaje
        }

        return null;
    }

    public List<LibreriasEntity> getListaFiltradaLibrerias() {
        return listaFiltradaLibrerias;
    }

    public void setListaFiltradaLibrerias(List<LibreriasEntity> listaFiltradaLibrerias) {
        this.listaFiltradaLibrerias = listaFiltradaLibrerias;
    }
    
    
}
