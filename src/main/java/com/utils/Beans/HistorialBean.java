/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.HistorialEntity;
import com.utils.Entities.UsuariosEntity;
import com.utils.Model.HistorialModel;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author Ernesto
 */
@Named(value = "historialBean")
@SessionScoped
public class HistorialBean implements Serializable {

    @EJB
    private HistorialModel historialModel;
    private HistorialEntity historial = new HistorialEntity();
    private List<HistorialEntity> listaHistorial;

    /**
     * Creates a new instance of HistorialBean
     */
    public HistorialBean() {

    }

    @PostConstruct
    public void init() {
//        historialdelUsuario = historialModel.listarHistorialporUsuario(LoginBean.usuario_en_sesion);

    }

    /**
     * @return the historialdelUsuario
     */
    public List<HistorialEntity> listarHistorialUsuario(UsuariosEntity usuario) {
        
        listaHistorial = historialModel.listarHistorialporUsuario(usuario);
        if (!historial.getBusqueda().trim().equals("")) {
            System.out.print("Esta buscando: " + historial.getBusqueda());
            listaHistorial = historialModel.listarHistorialporNombre(historial.getBusqueda(), usuario);
        } else if (historial.getFechaHora() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaString = formatter.format(historial.getFechaHora());
            System.out.print("Fecha   ->: " + fechaString);
            System.out.print("Usuario ->: " + usuario.getIdUsuario());
            System.out.print("Esta buscando la fecha: " + historial.getFechaHora());
             listaHistorial = historialModel.listarHistorialporFecha(fechaString, usuario.getIdUsuario());
        }
        //Limpiamos propiedades
        //historial.setBusqueda("");
        //historial.setFechaHora(null);
        return listaHistorial;
    }

//    public void listarporNombre() {
//        if (historial.getBusqueda().trim().equals("")) {
//            historialdelUsuario = historialModel.listarHistorialporUsuario(LoginBean.usuario_en_sesion);
//        } else {
//            System.out.print("Esta buscando: " + historial.getBusqueda());
//            historialdelUsuario = historialModel.listarHistorialporNombre(historial.getBusqueda());
//
//        }
//
//    }
    
    
    public void redireccion() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("../search/productos.xhtml");
    }
    /**
     * @return the historial
     */
    public HistorialEntity getHistorial() {
        return historial;
    }

    /**
     * @param historial the historial to set
     */
    public void setHistorial(HistorialEntity historial) {
        this.historial = historial;
    }

    public void listarporFecha(UsuariosEntity usuario){
            
        if(historial.getFechaHora()==null){
             listaHistorial = historialModel.listarHistorialporUsuario(usuario);
        }else{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String fechaString = formatter.format(historial.getFechaHora());
            System.out.print("Fecha   ->: " + fechaString);
            System.out.print("Usuario ->: " + usuario.getIdUsuario());
            
            System.out.print("Esta buscando la fecha: " + historial.getFechaHora());
             listaHistorial = historialModel.listarHistorialporFecha(fechaString, usuario.getIdUsuario());
            
        }
        
    } 
    
}
