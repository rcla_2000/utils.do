/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.ImagenesProductoEntity;
import com.utils.Entities.ProductosEntity;
import com.utils.Model.ImagenesModel;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author HP
 */
@Named(value = "imagenBean")
@RequestScoped
public class ImagenBean {

    @EJB
    private ImagenesModel imagenesModel;
    private ImagenesProductoEntity imagen = new ImagenesProductoEntity();
    private UploadedFile archivo;

    public ImagenBean() {
    }

    public void subirImagen() throws IOException {
        try {
            if (archivo != null) {

                imagen.setImagen(IOUtils.toByteArray(archivo.getInputstream()));

                if (imagenesModel.agregarImagen(imagen) > 0) {
                    System.out.println("vergon");
                } else {
                    //mensaje de error
                    System.out.println("ocurrio un error :v");
                }
                FacesMessage message = new FacesMessage("Succesful", archivo.getFileName() + " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } else {
                FacesMessage message = new FacesMessage("Error", "No sirve esto");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }

        } catch (Exception ex) {
            FacesMessage message = new FacesMessage("Error", "No sirve esto");
            FacesContext.getCurrentInstance().addMessage(null, message);
            Logger.getLogger(ImagenBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public StreamedContent getImagenProducto() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            try {
                int id = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("id"));
                this.imagen = imagenesModel.obtenerImagen(id);
                return new DefaultStreamedContent(new ByteArrayInputStream(this.imagen.getImagen()));
            } catch(Exception e)  {
                return new DefaultStreamedContent(this.getClass().getClassLoader().getResourceAsStream("default.jpg"));
            }
        }
    }

    public ImagenesProductoEntity getImagen() {
        return imagen;
    }

    public void setImagen(ImagenesProductoEntity imagen) {
        this.imagen = imagen;
    }

    public UploadedFile getArchivo() {
        return archivo;
    }

    public void setArchivo(UploadedFile archivo) {
        this.archivo = archivo;
    }

}
