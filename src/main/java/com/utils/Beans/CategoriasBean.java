/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.CategoriasEntity;
import com.utils.Model.CategoriasModel;
import com.utils.Utils.jsfUtil;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author HP
 */
@Named(value = "categoriasBean")
@SessionScoped
public class CategoriasBean implements Serializable {

    @EJB
    private CategoriasModel categoriasModel;
    private CategoriasEntity categoria = new CategoriasEntity();
    private List<CategoriasEntity> listaCategorias;
    private List<CategoriasEntity> listaFiltradaCategorias;

    public CategoriasBean() {
    }

    public List<CategoriasEntity> getListaCategorias() {
        listaCategorias = categoriasModel.listarCategorias();
        return listaCategorias;
    }

    public void agregarCategoria() throws IOException {
        if (categoriasModel.agregarCategoria(this.categoria) > 0) {
            jsfUtil.setFlashMessage("info", "Categoría agregada exitosamente");
            FacesContext.getCurrentInstance().getExternalContext().redirect("inicio.xhtml");
        } else {
            jsfUtil.setFlashMessage("error", "Ocurrió un error al agregar la categoría");
        }
    }

    public void modificarCategoria() throws IOException {
        if (categoriasModel.modificarCategoria(this.categoria) > 0) {
            jsfUtil.setFlashMessage("info", "Información de categoría actualizada exitosamente");
            FacesContext.getCurrentInstance().getExternalContext().redirect("inicio.xhtml");
        } else {
            jsfUtil.setFlashMessage("error", "Ocurrió un error al editar la información de la categoría");
        }
    }

    public void eliminarCategoria(int id) {
        CategoriasEntity c = categoriasModel.obtenerCategoria(id);
        if (c != null) {
            if (c.getProductosEntityList().size() == 0) {
                if (categoriasModel.eliminarCategoria(id) > 0) {
                    jsfUtil.setFlashMessage("info", "Categoría eliminada de forma exitosa");
                }
            } else {
                jsfUtil.setFlashMessage("error", "La categoría posee productos por ello no puede ser eliminada");
            }
        } else {
            jsfUtil.setFlashMessage("error", "No se encontró el registro de la categoría");
        }
    }

    public void obtenerCategoria(int id) throws IOException {
        CategoriasEntity c = categoriasModel.obtenerCategoria(id);
        if (c != null) {
            this.categoria = c;
            FacesContext.getCurrentInstance().getExternalContext().redirect("editar.xhtml");
        }else{
           jsfUtil.setFlashMessage("error", "No se encontró el registro de la categoría"); 
        }  
    }

    public void nuevaCategoria() throws IOException {
        this.categoria = new CategoriasEntity();
        FacesContext.getCurrentInstance().getExternalContext().redirect("agregar.xhtml");
    }

    public CategoriasEntity getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriasEntity categoria) {
        this.categoria = categoria;
    }

    public List<CategoriasEntity> getListaFiltradaCategorias() {
        return listaFiltradaCategorias;
    }

    public void setListaFiltradaCategorias(List<CategoriasEntity> listaFiltradaCategorias) {
        this.listaFiltradaCategorias = listaFiltradaCategorias;
    }

}
