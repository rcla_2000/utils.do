/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;
import com.utils.Entities.SolicitudesEntity;
import com.utils.Entities.TipoUsuariosEntity;
import com.utils.Entities.UsuariosEntity;
import com.utils.Model.SolicitudesModel;
import com.utils.Model.UsuarioModel;
import com.utils.Utils.CorreoMail;
import com.utils.Utils.Utils;
import com.utils.Utils.jsfUtil;
import com.utils.Utils.validation;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
/**
 *
 * @author Henry
 */
@Named(value = "solicitudesBean")
@SessionScoped
public class SolicitudesBean implements Serializable{

    /**
     * @return the tipo
     */
    public TipoUsuariosEntity getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoUsuariosEntity tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the usuario2
     */
    public UsuariosEntity getUsuario2() {
        return usuario2;
    }

    /**
     * @param usuario2 the usuario2 to set
     */
    public void setUsuario2(UsuariosEntity usuario2) {
        this.usuario2 = usuario2;
    }

    /**
     * @return the solicitud
     */
    public SolicitudesEntity getSolicitud() {
        return solicitud;
    }

    /**
     * @param solicitud the solicitud to set
     */
    public void setSolicitud(SolicitudesEntity solicitud) {
        this.solicitud = solicitud;
    }
    @EJB     
    private SolicitudesModel solicitudesModel;
    @EJB
    private UsuarioModel usuarioModel;
    List<SolicitudesEntity> lista;
    private SolicitudesEntity  solicitud = new SolicitudesEntity();
    private TipoUsuariosEntity tipo = new TipoUsuariosEntity();
    private UsuariosEntity usuario2 = new UsuariosEntity();
    
    
    public SolicitudesBean(){   
    }
    
    public List<SolicitudesEntity> getListaSolicitudes(){
        lista = solicitudesModel.listarOfertas();
        return lista;
    }
    
    public String AceptarSolicitud(String cod){
        System.out.print("Codigo Sol: " + cod);
        if(solicitudesModel.aprobarSolicitud(cod)>0){
            System.out.print("Entro");
            solicitud = new SolicitudesEntity();
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success","Librería aprobada");
            return "/dashboard/gestionarLibrerias?faces-redirect=true";
        }else{
            System.out.print("Ni entro");
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success","Error a la hora de registrar");
            return null;
        }
    }
    
    
    
     public String CancelarSolicitud(String cod, String correo, String nombreL, String nombre, String apellido){
        if(solicitudesModel.rechazarSolicitud(cod)>0){
            
            //Enviamos el Correo
            CorreoMail.CorreoNoAceptado(correo, nombreL, nombre, apellido);
            
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success","Librería rechazada");
            return "/dashboard/gestionarLibrerias?faces-redirect=true";
        }else{
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success","Error a la hora de rechazar");
            return null;
        }
    }
    
     public String insertarSolicitud(UsuariosEntity usuario){
        solicitud.setIdSolicitud(Utils.generarCodigoSolicitud());
        if(usuario == null || usuario.getIdUsuario().trim().equals("") ){
            System.out.print("Ingresa aquí");
            getTipo().setId(2);
            usuario2.setTipoUsuario(getTipo());
            usuario2.setApellidos(solicitud.getApellidos());
            usuario2.setNombres(solicitud.getNombres());
        String sexo = "masculino";
        char sex = sexo.charAt(0);
            usuario2.setSexo(sex);
        String fechaN="2000-01-01";  
        Date fechaNacimiento = new Date();  
            try {
                fechaNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(fechaN);
            } catch (ParseException ex) {
                Logger.getLogger(SolicitudesBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            usuario2.setFechaNacimiento(fechaNacimiento);
            usuario2.setEmail(solicitud.getEmail());
            usuario2.setDui(solicitud.getDui());
            usuario2.setDireccion(solicitud.getDireccion());
            usuario2.setTelefono(solicitud.getTelefono());
            usuario2.setIdUsuario(validation.Code(solicitud.getNombres(), solicitud.getApellidos()));
            usuario2.setPassword(validation.getMD5("password"));
        
        if(usuarioModel.agregarUsuario(this.usuario2)>0){
      //  System.out.print("Se inserto el usuario");
        solicitud.setUsuario(usuario2);
        }else{
     System.out.print("Ha ocurrido un error al insertar el usuario");
        }
        
        }else{
          //  System.out.print("Ingresa en otro lado");
        solicitud.setUsuario(usuario);
        }
       // System.out.print("Ingreso a nada");
        
      solicitud.setEstado(0);
       //System.out.print("Ver que trae la solicitud :\n1:" + solicitud.getApellidos()+ "\n2:" + solicitud.getDepartamento() + "\n3:" + solicitud.getDireccion()
       //+"\n4:"+ solicitud.getDireccionLibreria()+ "\n5:" + solicitud.getDui()+"\n6:" + solicitud.getEmail()+"\n7:"+solicitud.getIdSolicitud()+"\n8:"+solicitud.getLibreria()
       //+"\n9:" + solicitud.getMunicipio() + "\n10:"+solicitud.getNombres()+"\n11:"+solicitud.getTelefono());
       if(solicitudesModel.agregarSolicitud(solicitud)>0){
           System.out.print("Exito*");
           //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registrado exitosamente!"));
           FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success","Su solicitud se envió exitosamente");
           return "index?faces-redirect=true";
       }else{
           System.out.print("Algo anda mal");
           FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error","Esta solicitud esta en espera de ser aprobada");
           return null;
       }
    }

    public void obtenerSolicitud(String id) throws IOException{
        SolicitudesEntity s = solicitudesModel.obtenerSolicitud(id);
        if(s != null){
            this.solicitud = s;
            FacesContext.getCurrentInstance().getExternalContext().redirect("detalles.xhtml");
        }else{
            jsfUtil.setFlashMessage("error", "No se pudo obtener el registro de la solicitud");
        }
    }
}
