/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.ListasEntity;
import com.utils.Model.ListaModel;
import com.utils.Model.UsuarioModel;
import com.utils.Utils.validation;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author jonat
 */
@Named(value = "listaBean")
@SessionScoped
public class ListaBean implements Serializable {

    @EJB
    private ListaModel listaModel;
    private ListasEntity lista = new ListasEntity();
    @EJB
    private UsuarioModel usuarioModel;
    List<ListasEntity> listaListas;
   
    
    public ListaBean() {
    }
    
    //Listar todas las listas
     public List<ListasEntity> getListaListas(String codigo) {
        listaListas = listaModel.listarListas(usuarioModel.buscarUsuario(codigo));
        return listaListas;
    }
     
     //Insertar lista
     public String insertarLista(String codeUser) {
        lista.setIdLista(validation.CodeList());
        lista.setIdUsuario(usuarioModel.buscarUsuario(codeUser));
        if (listaModel.agregarLista(this.lista) > 0){
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("exito", "La lista se agrego exitosamente!");
            lista = new ListasEntity();
            return "/dashboard/dashboardClient.xhtml" + "?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "No se pudo agregar la lista");
            lista = new ListasEntity();
            return "/dashboard/dashboardClient.xhtml" + "?faces-redirect=true";
        }
    } 
    
    //Eliminar lista
    public String eliminarLista(String codigo) {
        if (listaModel.eliminarLista(codigo) > 0){
             FacesContext.getCurrentInstance().getExternalContext().getFlash().put("exito", "La lista se elimino exitosamente!");
             return null;
        }else{
             FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "La lista no fue eliminada");
             return null;
        }
    }
    
    //Buscar Lista 
     public String obtenerLista(String codigo) {
        lista = listaModel.buscarLista(codigo);
        return "/dashboard/dashboardClient.xhtml" + "?faces-redirect=true";
    }
     
    //Modificar lista
      public String modificarLista() {
        if (listaModel.modificarLista(lista) > 0) {
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registrado exitosamente!"));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success", "La lista fue modificada exitosamente");
            lista = new ListasEntity();
            return "/dashboard/dashboardClient.xhtml" + "?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "La lista no fue modificada");
            return "/dashboard/dashboardClient.xhtml" + "?faces-redirect=true";
        }
    }
     

    public ListasEntity getLista() {
        return lista;
    }

    public void setLista(ListasEntity lista) {
        this.lista = lista;
    }
    
    
    
}
