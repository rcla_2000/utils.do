/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.TipoUsuariosEntity;
import com.utils.Entities.UsuariosEntity;
import com.utils.Model.UsuarioModel;
import com.utils.Utils.jsfUtil;
import com.utils.Utils.validation;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Jonathan
 */
@Named(value = "usuarioBean")
@SessionScoped
public class UsuarioBean implements Serializable {

    @EJB
    private UsuarioModel usuarioModel;
    private UsuariosEntity usuario = new UsuariosEntity();
    List<UsuariosEntity> listaUsuarios;
    private String nuevaContra = "";
    //private String confirmar = "";
    private PieChartModel pastelModel1;
    private PieChartModel pastelModel2;

    public UsuarioBean() {
    }

    @PostConstruct
    public void init() {
        graficoEdades();
        graficoGenero();
    }

    public List<UsuariosEntity> getListaUsuarios() {
        listaUsuarios = usuarioModel.listarUsuarios();
        return listaUsuarios;
    }

    public String insertarUsuario() {
        TipoUsuariosEntity tipo = new TipoUsuariosEntity();
        tipo.setId(3);
        usuario.setTipoUsuario(tipo);
        usuario.setIdUsuario(validation.Code(usuario.getNombres(), usuario.getApellidos()));
        usuario.setPassword(validation.getMD5(usuario.getPassword()));

        if (usuarioModel.agregarUsuario(this.usuario) > 0) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("exito", "Fuiste registrado exitosamente");
            return "iniciar?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Este correo ya fue registrado");
            return null;
        }
    }

    public String modificarUsuario() {

        if (!nuevaContra.isEmpty()) {
            usuario.setPassword(validation.getMD5(nuevaContra));
        }

        if (usuarioModel.modificarUsuario(usuario) > 0) {
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Registrado exitosamente!"));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("success", "Fuiste registrado exitosamente");
            usuario = new UsuariosEntity();
            return "/cliente/dashboardClient?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Este correo ya fue registrado");
            return "/cliente/UserDate?faces-redirect=true";
        }
    }

    public void graficoGenero() {
        pastelModel1 = new PieChartModel();
        pastelModel1.set("Hombres", usuarioModel.cantidadUsuariosGenero('m'));
        pastelModel1.set("Mujeres", usuarioModel.cantidadUsuariosGenero('f'));
        pastelModel1.setTitle("Porcentaje de usuarios hombres y mujeres");
        pastelModel1.setLegendPosition("no");
        pastelModel1.setShowDataLabels(true);
        pastelModel1.setDiameter(300);
        pastelModel1.setShadow(false);
    }

    public void graficoEdades() {
        List<Long> edades = usuarioModel.obtenerEdadesUsuarios();
        pastelModel2 = new PieChartModel();
        int n1 = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        for (Long edad : edades) {
            if (edad >= 0 && edad <= 12) {
                n1++;
            } else if (edad >= 13 && edad <= 17) {
                n2++;
            } else if (edad >= 18 && edad <= 29) {
                n3++;
            } else if (edad >= 30 && edad <= 59) {
                n4++;
            } else {
                n5++;
            }
        }

        pastelModel2.set("Niños (0-12) años", n1);
        pastelModel2.set("Adolescentes (13-17) años", n2);
        pastelModel2.set("Adulto Jóven (18-29) años", n3);
        pastelModel2.set("Adultos (30-59) años", n4);
        pastelModel2.set("Adulto Mayor (60 + ) años", n5);
        pastelModel2.setTitle("Porcentaje de edades de usuarios");
        pastelModel2.setLegendPosition("se");
        pastelModel2.setShowDataLabels(true);
        pastelModel2.setDiameter(250);
        pastelModel2.setShadow(false);
    }

    public String obtenerUsuario(String codigo) {
        usuario = usuarioModel.buscarUsuario(codigo);
        return "/dashboard/registrarAdmin?faces-redirect=true";
    }

    public void obtenerUsuariodeBusqueda(String codigo) {
        usuario = usuarioModel.buscarUsuario(codigo);

    }

    public void obtenerUsuarioSesion(String codigo) throws IOException {
        usuario = usuarioModel.buscarUsuario(codigo);
        FacesContext.getCurrentInstance().getExternalContext().redirect("../cliente/UserDate.xhtml");
    }

    public String eliminarUsuario(String codigo) {

        if (usuarioModel.eliminarUsuario(codigo) > 0) {

        }

        return null;
    }

    public Date getFechaActual() {
        return new Date();
    }

    public UsuariosEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuariosEntity usuario) {
        this.usuario = usuario;
    }

    public String getNuevaContra() {
        return nuevaContra;
    }

    public void setNuevaContra(String nuevaContra) {
        this.nuevaContra = nuevaContra;
    }

    public PieChartModel getPastelModel1() {
        return pastelModel1;
    }

    public void setPastelModel1(PieChartModel pastelModel1) {
        this.pastelModel1 = pastelModel1;
    }

    public PieChartModel getPastelModel2() {
        return pastelModel2;
    }

    public void setPastelModel2(PieChartModel pastelModel2) {
        this.pastelModel2 = pastelModel2;
    }

}
