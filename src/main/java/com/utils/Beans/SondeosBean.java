///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.utils.Beans;
//
//import java.sql.SQLException;
//import javax.ejb.EJB;
//import javax.inject.Named;
//import javax.enterprise.context.RequestScoped;
//import javax.faces.context.FacesContext;
//import javax.faces.event.AjaxBehaviorEvent;
//import javax.servlet.http.HttpServletRequest;
//import com.utils.Entities.SondeosEntity;
//
//import com.utils.Model.SondeosModel;
//import java.io.IOException;
//import java.io.Serializable;
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//import javax.faces.context.ExternalContext;
//
//import javax.enterprise.context.SessionScoped;
//import javax.faces.application.FacesMessage;
//import org.primefaces.model.chart.Axis;
//import org.primefaces.model.chart.AxisType;
//import org.primefaces.model.chart.BarChartModel;
//import org.primefaces.model.chart.BarChartSeries;
//import org.primefaces.model.chart.ChartSeries;
//import org.primefaces.model.chart.PieChartModel;
//
///**
// *
// * @author Ernesto
// */
//@Named(value = "sondeosBean")
//
//@RequestScoped
//public class SondeosBean implements Serializable{
//
//
//    @EJB
//    private SondeosModel sondeosModel;
//
//    private List<SondeosEntity> listaSondeo;
//    private SondeosEntity sondeo = new SondeosEntity();
//
//
//    private List<SondeosEntity> listaMunicipios;
//    private List<SondeosEntity> listaDepartamentos;
//
//
//    private List<SondeosEntity> listaComparacion;
//    private BarChartModel barra;
//    private PieChartModel pastel;
//   
//    public SondeosBean() {
//    }
//
//    public List<SondeosEntity> getListaSondeo() {
//        listaSondeo = sondeosModel.listarSondeo();
//        if (sondeo.getDepartamento().equals("") == false) {
//            listaSondeo = sondeosModel.listarPorDepartamento(sondeo.getDepartamento());
//
//            
//       }else if(sondeo.getMunicipio().equals("") == false){
//           System.out.print("El municipio escogido es: " + sondeo.getMunicipio());
//            listaSondeo = sondeosModel.listarPorMunicipio(sondeo.getMunicipio());
//       
//        }else if(sondeo.getDepartamento().equals("") == false && sondeo.getMunicipio().equals("") == false){
//            //Si selecciona un departamento que se muestre por municipio también
//           System.out.print("El departamento/municipio escogido es: " + sondeo.getMunicipio());
//            listaSondeo = sondeosModel.listarPorMunicipio(sondeo.getMunicipio());
//       
//        }else if(sondeo.getProducto().equals("") == false && sondeo.getEstablecimiento().equals("")==true){
//            //Mostrar la lista de productos por nombre
//            listaSondeo = sondeosModel.listarporNombre(sondeo.getProducto());
//        }else if(sondeo.getEstablecimiento().equals("") == false && sondeo.getProducto().equals("")==false){
//            //Mostrar los productos del establecimiento seleccionado y producto escrito
//            System.out.print("Vamos por buen camino :d");
//            listaSondeo = sondeosModel.listarporEstablecimiento_Producto(sondeo.getEstablecimiento(), sondeo.getProducto());
//        }else if(sondeo.getEstablecimiento().equals("") == false){
//            //Mostrar los productos del establecimiento seleccionado
//            System.out.print("Vamos por buen camino");
//            listaSondeo = sondeosModel.listarporEstablecimiento(sondeo.getEstablecimiento());
//        }else if(sondeo.getIdSondeo()!=0){
//            listaSondeo = sondeosModel.listarPorId(sondeo.getIdSondeo());
//
//            
//        }
//        return listaSondeo;
//    }
//
//  
//    public void generarGraficoBarra(){
//        barra = new BarChartModel();
//                
//        for(SondeosEntity s: sondeosModel.comparacionProductos(sondeo.getProducto())){
//            ChartSeries serie = new BarChartSeries();
//            serie.setLabel(s.getEstablecimiento());
//            serie.set(s.getProducto(), s.getPrecio());
//            barra.addSeries(serie);
//        }
//        
//        barra.setTitle("Precios del producto | " + sondeo.getProducto() + " | en distintas librerías");
//        barra.setLegendPosition("ne");
//        barra.setAnimate(true);
//        barra.setShowDatatip(true);
//        barra.setShowPointLabels(true);
//        
//        Axis xAxis = barra.getAxis(AxisType.X);
//        xAxis.setLabel("Librerías");
//        Axis yAxis = barra.getAxis(AxisType.Y);
//        yAxis.setLabel("Precios en dólares estadounidenses ($)");
//        yAxis.setMin(0.01);
//        yAxis.setMax(5);
//    }
//    
//
//
//    public void listarPorDepartamento(){
//        String departamento = sondeo.getDepartamento();
//        listaSondeo = sondeosModel.listarPorDepartamento(departamento);
//    }
//    
//    public void generarGraficoPastel(){
//        pastel = new PieChartModel();
//        for(SondeosEntity s: sondeosModel.comparacionProductos(sondeo.getProducto())){
//            pastel.set(s.getEstablecimiento(), s.getPrecio());
//        }
//        
//        pastel.setTitle("Precios del producto | " + sondeo.getProducto() + " | en distintas librerías");
//        pastel.setLegendPosition("w");
//        pastel.setDiameter(450);
//        pastel.setShowDatatip(true);
//        pastel.setShowDataLabels(true);
//        pastel.setDataFormat("value");
//        pastel.setDataLabelFormatString("$ %.2f");
//    }
//    
//    public void redirigir() throws IOException{
//        FacesContext.getCurrentInstance().getExternalContext().redirect("graficador.xhtml");
//
//    }
//
//    public void obtenerporNombre(){
//        String producto = sondeo.getProducto();
//        System.out.println("Usted esta buscando: " + producto);
//        listaSondeo = sondeosModel.listarporNombre(producto);
//    }
//    
//
//    /**
//     * @return the sondeo
//     */
//    public SondeosEntity getSondeo() {
//        return sondeo;
//    }
//
//    /**
//     * @param sondeo the sondeo to set
//     */
//    public void setSondeo(SondeosEntity sondeo) {
//        this.sondeo = sondeo;
//
//    }
//
//    public List<SondeosEntity> getListaComparacion() {
//        return listaComparacion;
//    }
//
//    public void setListaComparacion(List<SondeosEntity> listaComparacion) {
//        this.listaComparacion = listaComparacion;
//    }
//
//    public BarChartModel getBarra() {
//        return barra;
//    }
//
//    public void setBarra(BarChartModel barra) {
//        this.barra = barra;
//    }
//
//    public PieChartModel getPastel() {
//        return pastel;
//    }
//
//    public void setPastel(PieChartModel pastel) {
//        this.pastel = pastel;
//    }
//    
//    
//
//    
//    
//    /**
//     * @return the listaMunicipios
//     */
//    
//    public List<SondeosEntity> getListaMunicipios() {
//        
//        if(sondeo.getDepartamento().equals("")==true){
//            System.out.print("NO selecciono departamento se muestra por defecto");
//        listaMunicipios = sondeosModel.listaMunicipios();
//        }else{
//            System.out.print("Ha seleccionado un departamento");
//        listaMunicipios = sondeosModel.listaMunicipios(sondeo.getDepartamento());
//        }
//        
//        
//        return listaMunicipios;
//    }
//
//    /**
//     * @param listaMunicipios the listaMunicipios to set
//     */
//    public void setListaMunicipios(List<SondeosEntity> listaMunicipios) {
//        this.listaMunicipios = listaMunicipios;
//    }
//
//    /**
//     * @return the listaDepartamentos
//     */
//    public List<SondeosEntity> getListaDepartamentos() {
//        listaDepartamentos = sondeosModel.listaDepartamentos();
//        return listaDepartamentos;
//    }
//
//    /**
//     * @param listaDepartamentos the listaDepartamentos to set
//     */
//    public void setListaDepartamentos(List<SondeosEntity> listaDepartamentos) {
//        this.listaDepartamentos = listaDepartamentos;
//    }
//    
//    public String seleccionarLibreria(String nombre) throws IOException{
//        sondeo.setEstablecimiento(nombre);
//    System.out.print("Establecimiento sel:" + nombre);
//    return "libreria" + "";
//    
//    //FacesContext.getCurrentInstance().getExternalContext().redirect("libreria.xhtml");
//    
//    }
//    
//    public String seleccionarProducto(int idSondeo) throws IOException{
//        sondeo.setIdSondeo(idSondeo);
//    System.out.print("Producto sel:" + idSondeo);
//    return "producto" + "";
//    
//    //FacesContext.getCurrentInstance().getExternalContext().redirect("libreria.xhtml");
//    
//    }
//    
//    public void validarLibreria() throws IOException {
//        
//    if (sondeo.getEstablecimiento().equals("")==true) {
//        FacesContext.getCurrentInstance().getExternalContext().redirect("buscador.xhtml");
//    }
//    }
//    
//        public void validarProducto() throws IOException {
//        
//    if (sondeo.getIdSondeo()==0) {
//        FacesContext.getCurrentInstance().getExternalContext().redirect("buscador.xhtml");
//    }
//    }
//    
//    
//
//
//        
//}
