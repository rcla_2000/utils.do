/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.HistorialEntity;
import com.utils.Entities.ImagenesProductoEntity;
import com.utils.Entities.LibreriaProductoEntity;
import com.utils.Entities.LibreriasEntity;
import com.utils.Entities.ProductosEntity;
import com.utils.Model.ImagenesModel;
import com.utils.Model.ProductosModel;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import com.utils.Utils.jsfUtil;
import com.utils.Entities.UsuariosEntity;
import com.utils.Model.HistorialModel;
import com.utils.Model.UsuarioModel;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
@Named(value = "productosBean")
@SessionScoped
public class ProductosBean implements Serializable {

    @EJB
    private ImagenesModel imagenesModel;

    @EJB
    private ProductosModel productosModel;

    @EJB
    private HistorialModel historialModel;

    @EJB
    private UsuarioModel usuarioModel;

    private ProductosEntity producto = new ProductosEntity();
    private HistorialEntity historial = new HistorialEntity();
    private UsuariosEntity usuario = new UsuariosEntity();
    private LibreriaProductoEntity libreriaProducto = new LibreriaProductoEntity();
    private List<ProductosEntity> listaProductos;
    private List<ProductosEntity> listaProductosSinImagen;
    private List<ProductosEntity> listaProductosPorLibreria;
    private List<LibreriaProductoEntity> listaProductosPorAdministrador;
    private List<LibreriaProductoEntity> listaPreciosProducto;
    private List<LibreriaProductoEntity> listaPreciosMayores;
    private List<LibreriaProductoEntity> listaPreciosMenores;
    private List<LibreriaProductoEntity> listaPersonalizada;
    private List<LibreriaProductoEntity>  listaFiltradaMisProductos;
    private List<String> libreriasComparacion;
    private BarChartModel barra;
    private ImagenesProductoEntity imagen = new ImagenesProductoEntity();
    private UploadedFile archivo;
    private int numeroArchivos;

    public ProductosBean() {
    }

    @PostConstruct
    public void init() {
        listaProductos = productosModel.listarProductos();
        listaPersonalizada = null;
    }

    public List<ProductosEntity> getListaProductosSinImagen() {
        listaProductosSinImagen = productosModel.listaProductosSinImagen();
        return listaProductosSinImagen;
    }

    public List<ProductosEntity> listarProductosPorLibreria(String idLibreria) {
        listaProductosPorLibreria = productosModel.listarProductosPorLibreria(idLibreria);
        return listaProductosPorLibreria;
    }

    public List<LibreriaProductoEntity> listarProductosPorAdministrador(UsuariosEntity administrador) {
        listaProductosPorAdministrador = productosModel.listarProductosPorAdministrador(administrador);
        return listaProductosPorAdministrador;
    }

    public void nuevoProducto() throws IOException {
        this.producto = new ProductosEntity();
        this.libreriaProducto = new LibreriaProductoEntity();
        this.imagen = new ImagenesProductoEntity();
        this.archivo = null;
        FacesContext.getCurrentInstance().getExternalContext().redirect("agregar.xhtml");
    }

    public void listarProductosPorNombre() {
        if (producto.getNombre().equals("") || producto.getNombre().trim().equals("")) {
            listaProductos = productosModel.listarProductos();
        } else {
            listaProductos = productosModel.listarProductosPorNombre(producto.getNombre());
            String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idUsuario");
            usuario = usuarioModel.buscarUsuario(id);
            if (usuario != null) {
                historial.setBusqueda(producto.getNombre());
                historial.setFechaHora(new Date());
                historial.setIdUsuario(usuario);
                historialModel.agregarAlHistorial(historial);
            }
        }
    }

    public void listarProductosPorCategoria() {
        if (producto.getIdCategoria().getIdCategoria() == 0) {
            listaProductos = productosModel.listarProductos();
        } else {

            listaProductos = productosModel.listarProductosPorCategoria(producto.getIdCategoria());

        }
    }

    public void listarPreciosProductoPorDepartamento() {
        if (libreriaProducto.getIdLibreria().getDepartamento().equals("-1")) {
            listaPreciosProducto = productosModel.listarPreciosProducto(producto);
        } else {

            listaPreciosProducto = productosModel.listarPreciosProductoPorDepartamento(producto, libreriaProducto.getIdLibreria().getDepartamento());
        }
    }

    public void listarPreciosProductoPorMunicipio() {
        if (libreriaProducto.getIdLibreria().getMunicipio().equals("-1")) {
            listaPreciosProducto = productosModel.listarPreciosProductoPorDepartamento(producto, libreriaProducto.getIdLibreria().getDepartamento());
        } else {

            listaPreciosProducto = productosModel.listarPreciosProductoPorMunicipio(producto, libreriaProducto.getIdLibreria().getMunicipio());
        }
    }

    public void comparacionPersonalizada() {
        List<LibreriasEntity> libreriasSeleccionadas = new ArrayList<>();
        for (String l : libreriasComparacion) {
            libreriasSeleccionadas.add(new LibreriasEntity(l));
        }
        listaPreciosProducto = productosModel.listarComparacionPersonalizada(producto, libreriasSeleccionadas);
    }

    public void agregarProducto() throws IOException {
        this.producto.setIdProducto(jsfUtil.generarCodigo());
        this.producto.setStock(1);
        this.imagen.setIdProducto(producto);
        subirImagen();
        this.producto.setImagenesProductoEntity(imagen);
        if (productosModel.agregarProducto(this.producto) > 0) {
            this.libreriaProducto.setIdProducto(this.producto);
            int n = productosModel.agregarProductoLibreria(libreriaProducto);
            if (n == 1) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("inicio.xhtml");
            }
        }
    }

    public void modificarProducto() throws IOException {
        subirImagen();
        this.producto.setImagenesProductoEntity(this.imagen);
        if (productosModel.modificarProducto(this.producto) > 0) {
            int n = productosModel.modificarProductoLibreria(libreriaProducto);
            if (n == 1) {
                System.out.println("Modificado exitosamente");
                FacesContext.getCurrentInstance().getExternalContext().redirect("inicio.xhtml");
            }
        }
    }

    public void eliminarProducto(String id) {
        ProductosEntity p = productosModel.obtenerProducto(id);
        if (p != null) {
            if (productosModel.eliminarProducto(id) > 0) {
                //mensaje ok
            } else {
                //mensaje error
            }
        } else {
            //mensaje no se encontro
        }
    }

    public void obtenerProductoCompleto(String idProducto, int idLibreriaProducto, String ruta) throws IOException {
        ProductosEntity p = productosModel.obtenerProducto(idProducto);
        LibreriaProductoEntity lp = productosModel.obtenerProductoLibreria(idLibreriaProducto);
        if (p != null && lp != null) {
            this.producto = p;
            this.libreriaProducto = lp;
            System.out.println(producto);
            System.out.println(libreriaProducto);
            System.out.println("MI proc: " + imagenesModel.obtenerImagenProducto(producto));
            this.imagen = imagenesModel.obtenerImagenProducto(producto);
            this.archivo = null;
            this.numeroArchivos = 0;
            FacesContext.getCurrentInstance().getExternalContext().redirect(ruta + ".xhtml");
        }
        //mensaje de error
    }

    public void generarGraficoBarra() throws IOException {
        barra = new BarChartModel();

        for (LibreriaProductoEntity l : listaPreciosProducto) {
            ChartSeries serie = new ChartSeries();
            serie.setLabel(l.getIdLibreria().getNombre());
            serie.set("Librerías", l.getPrecio());
            barra.addSeries(serie);
        }

        barra.setTitle("Precios del producto | " + producto.getNombre() + " | en distintas librerías");
        barra.setLegendPosition("ne");
        barra.setAnimate(true);
        barra.setShowDatatip(false);
        barra.setShowPointLabels(true);

        Axis xAxis = barra.getAxis(AxisType.X);
        Axis yAxis = barra.getAxis(AxisType.Y);
        yAxis.setLabel("Precios USD ($)");
        yAxis.setMin(0);
        yAxis.setMax(5);
        yAxis.setTickFormat("$ %'.2f");

        FacesContext.getCurrentInstance().getExternalContext().redirect("graficador.xhtml");
    }

    public List<LibreriaProductoEntity> getListaPersonalizada() {
        return listaPersonalizada;
    }

    public void obtenerProducto(String id) throws IOException {
        this.producto = productosModel.obtenerProducto(id);
        listaPreciosProducto = productosModel.listarPreciosProducto(producto);
        FacesContext.getCurrentInstance().getExternalContext().redirect("detallesProducto.xhtml");
    }

    public List<LibreriaProductoEntity> getListaPreciosMayores() {
        listaPreciosMayores = productosModel.listarPreciosMayores(producto);
        return listaPreciosMayores;
    }

    public List<LibreriaProductoEntity> getListaPreciosMenores() {
        listaPreciosMenores = productosModel.listarPreciosMenores(producto);
        return listaPreciosMenores;
    }

    public void subirImagen() throws IOException {
        try {
            if (this.numeroArchivos > 0) {
                System.out.println("El archivo si es distinto de null");
                this.imagen.setImagen(IOUtils.toByteArray(archivo.getInputstream()));
            } else {
                FacesMessage message = new FacesMessage("Error", "Ocurrió un problema al cargar el archivo de imagen.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage("Error", "Ocurrió un problema al cargar el archivo de imagen.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            Logger.getLogger(ImagenBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int modificarImagen() throws IOException {
        try {
            if (archivo != null) {
                imagen.setImagen(IOUtils.toByteArray(archivo.getInputstream()));
            }
            if (imagenesModel.modificarImagen(imagen) > 0) {
                FacesMessage message = new FacesMessage("Succesful", "(archivo de imagen) adjuntado exitosamente");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return 1;
            } else {
                FacesMessage message = new FacesMessage("Error", "Ocurrió un problema al cargar el archivo de imagen.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return 0;
            }
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage("Error", "Ocurrió un problema al cargar el archivo de imagen.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            Logger
                    .getLogger(ImagenBean.class
                            .getName()).log(Level.SEVERE, null, ex);
            return 0;
        }

    }

    public StreamedContent getImagenProducto() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            try {
                int id = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("id"));
                this.imagen = imagenesModel.obtenerImagen(id);
                return new DefaultStreamedContent(new ByteArrayInputStream(this.imagen.getImagen()));
            } catch (Exception e) {
                return new DefaultStreamedContent(this.getClass().getClassLoader().getResourceAsStream("default.jpg"));
            }
        }
    }

    public ImagenesProductoEntity getImagen() {
        return imagen;
    }

    public void setImagen(ImagenesProductoEntity imagen) {
        this.imagen = imagen;
    }

    public UploadedFile getArchivo() {
        return archivo;
    }

    public void setArchivo(UploadedFile archivo) {
        this.archivo = archivo;
    }

    public List<ProductosEntity> getListaProductos() {
        return listaProductos;
    }

    public List<LibreriaProductoEntity> getListaPreciosProducto() {
        return listaPreciosProducto;
    }

    public ProductosEntity getProducto() {
        return producto;
    }

    public void setProducto(ProductosEntity producto) {
        this.producto = producto;
    }

    public LibreriaProductoEntity getLibreriaProducto() {
        return libreriaProducto;
    }

    public void setLibreriaProducto(LibreriaProductoEntity libreriaProducto) {
        this.libreriaProducto = libreriaProducto;
    }

    public List<String> getLibreriasComparacion() {
        return libreriasComparacion;
    }

    public void setLibreriasComparacion(List<String> libreriasComparacion) {
        this.libreriasComparacion = libreriasComparacion;
    }

    public BarChartModel getBarra() {
        return barra;
    }

    public void setBarra(BarChartModel barra) {
        this.barra = barra;
    }

    public String getDatatipFormat() {
        return "<span style=\"display:none;\">%s</span><span>%s</span>";
    }

    public List<LibreriaProductoEntity> getListaFiltradaMisProductos() {
        return listaFiltradaMisProductos;
    }

    public void setListaFiltradaMisProductos(List<LibreriaProductoEntity> listaFiltradaMisProductos) {
        this.listaFiltradaMisProductos = listaFiltradaMisProductos;
    }

    public int getNumeroArchivos() {
        return numeroArchivos;
    }

    public void setNumeroArchivos(int numeroArchivos) {
        this.numeroArchivos = numeroArchivos;
    }
    
    
}
