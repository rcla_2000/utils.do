/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.ListaProductosEntity;
import com.utils.Model.ListaModel;
import com.utils.Model.ListaProductosModel;
import com.utils.Model.ProductosModel;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;

/**
 *
 * @author jonat
 */
@Named(value = "listaProductosBean")
@SessionScoped
public class ListaProductosBean implements Serializable {

    @EJB
    private ProductosModel productosModel;

    @EJB
    private ListaModel listaModel;

    @EJB
    private ListaProductosModel listaProductosModel;
    private List<ListaProductosEntity> lista;
    private ListaProductosEntity producto;
    private String codigoLista;
    
    
    public ListaProductosBean() {
        producto = new ListaProductosEntity();
    }
    
     public List<ListaProductosEntity> getListaProductos(String codigo) {
        lista = listaProductosModel.listarListas(codigo);
        return lista;
    }
     
    //Eliminar lista
    public String eliminarProducto(int codigo) {
        if (listaProductosModel.eliminarProducto(codigo)>0){
             FacesContext.getCurrentInstance().getExternalContext().getFlash().put("exito", "El producto se elimino exitosamente!");
             return "";
        }else{
             FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "El producto no fue eliminado");
             return "";
        }
    }
    
    //Insertar lista
     public String insertarProducto(String codigo) {
         producto.setId(1);
         producto.setIdLista(listaModel.buscarLista(codigoLista));
         producto.setIdProducto(productosModel.obtenerProducto(codigo));
         producto.setCantidad(1);
        if (listaProductosModel.agregarLista(producto) > 0){
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("exito", "El producto se agrego exitosamente!");
            producto = new ListaProductosEntity();
            codigoLista = "";
            return "/search/buscador.xhtml" + "?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "No se pudo agregar el producto");
            producto = new ListaProductosEntity();
            return null;
        }
    }

    public ListaProductosEntity getProducto() {
        return producto;
    }

    public void setProducto(ListaProductosEntity producto) {
        this.producto = producto;
    }

    public String getCodigoLista() {
        return codigoLista;
    }

    public void setCodigoLista(String codigoLista) {
        this.codigoLista = codigoLista;
    }
    
    
    
}
