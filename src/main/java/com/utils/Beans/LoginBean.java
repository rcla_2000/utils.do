/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Beans;

import com.utils.Entities.UsuariosEntity;
import com.utils.Utils.Utils;
import com.utils.Model.UsuarioModel;
import com.utils.Utils.jsfUtil;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;

/**
 *
 * @author HP
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    @EJB
    private UsuarioModel usuariosModel;
    private UsuariosEntity usuario = new UsuariosEntity();

    public LoginBean() {
    }

    public String validar() {
        for (UsuariosEntity u : usuariosModel.listarUsuarios()) {
            if (u.getEmail().equals(this.usuario.getEmail())
                    && u.getPassword().equals(Utils.getMD5(this.usuario.getPassword()))) {

                this.usuario = u;
                jsfUtil.setFlashMessage("info", "Bienvenido/a " + u.getNombres() + " " + u.getApellidos());
                if (u.getTipoUsuario().getId().equals(1)) {
                    return "/administrador/dashboard.xhtml" + "?faces-redirect=true";
                } else if (u.getTipoUsuario().getId().equals(2)) {
                    return "/administrador-libreria/dashboard.xhtml" + "?faces-redirect=true";
                } else if (u.getTipoUsuario().getId().equals(3)) {
                    return "/usuarios/dashboard.xhtml" + "?faces-redirect=true";
                }
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El correo electrónico o contraseña ingresada son incorrectos", "Error"));

        return "";
    }

    public boolean verificarSesion() {

        boolean sesionIniciada = false;

        for (UsuariosEntity u : usuariosModel.listarUsuarios()) {
            if ((u.getEmail().equals(this.usuario.getEmail())
                    && u.getPassword().equals(this.usuario.getPassword()))) {

                sesionIniciada = true;
            }
        }
        return sesionIniciada;
    }

    public void security(int role) {

        boolean sesion = verificarSesion();

        if ((this.usuario == null || this.usuario.getTipoUsuario().getId() == null) || (sesion == false || (this.usuario.getTipoUsuario().getId().equals(role) == false))) {

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();

            try {
                ec.redirect("../usuarios/iniciar.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void sesionRequerida() {
        boolean sesion = verificarSesion();
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        if (!sesion) {
            try {
                ec.redirect("../usuarios/iniciar.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void mostrarLogin() {

        boolean sesion = verificarSesion();

        if ((this.usuario != null && this.usuario.getTipoUsuario().getId() != null) && (sesion == true)) {

            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();

            try {
                if (this.usuario.getTipoUsuario().getId().equals(1)) {
                    ec.redirect("../administrador/dashboard.xhtml");
                } else if (this.usuario.getTipoUsuario().getId().equals(2)) {
                    ec.redirect("../administrador-libreria/dashboard.xhtml");
                } else if (this.usuario.getTipoUsuario().getId().equals(3)) {
                    ec.redirect("../usuarios/dashboard.xhtml");
                }

            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public String logout() {
        this.usuario = new UsuariosEntity();
        return "/faces/usuarios/iniciar.xhtml" + "?faces-redirect=true";
    }

    public UsuariosEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuariosEntity usuario) {
        this.usuario = usuario;
    }

    public String recuperarCorreo() {

        for (UsuariosEntity u : usuariosModel.listarUsuarios()) {
            if (u.getEmail().equals(this.usuario.getEmail())) {
                //Si existe el correo...
                usuariosModel.modificarPass(this.usuario.getEmail());
                System.out.println(this.usuario.getEmail());

//
                return "iniciar?faces-redirect=true";
            }
        }
        System.out.println("No existe este correo");
        FacesContext.getCurrentInstance().addMessage("error", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Correo electr�nico incorrecto", "Error"));
        return "";
    }

    
    public String mostrar_dashboard(int id) throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
    if(id==1){
       // ec.redirect("/faces/administrador/dashboard.xhtml");
        return "/faces/administrador/dashboard.xhtml" + "?faces-redirect=true";
        
    }else if(id == 2){
     //   ec.redirect("/faces/administrador-libreria/dashboard.xhtml");
     return "/faces/administrador-libreria/dashboard.xhtml" + "?faces-redirect=true";
    
    }
    else if(id == 3){
    //    ec.redirect("/faces/usuario/dashboard.xhtml");
    return "/faces/usuarios/dashboard.xhtml" + "?faces-redirect=true";
    }
    return "hola.xhtml";
    }
}
