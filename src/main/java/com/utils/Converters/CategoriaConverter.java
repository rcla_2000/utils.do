///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.utils.Converters;
//
//import com.utils.Entities.CategoriasEntity;
//import com.utils.Model.CategoriasModel;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.faces.component.UIComponent;
//import javax.faces.context.FacesContext;
//import javax.faces.convert.Converter;
//import javax.faces.convert.FacesConverter;
//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;
//
///**
// *
// * @author HP
// */
//@FacesConverter(forClass = CategoriasEntity.class)
//public class CategoriaConverter implements Converter {
//
//    CategoriasModel categoriasModel = lookupCategoriasModelBean();
//
//    @Override
//    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
//        int value;
//        try {
//            if (string == null || string.isEmpty()) {
//                return null;
//            }
//            value = Integer.parseInt(string);
//        } catch (Exception ex) {
//            return null;
//        }
//
//        return categoriasModel.obtenerCategoria(value);
//    }
//
//    @Override
//    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
//        if (o == null) {
//            return "";
//        }
//
//        if (o instanceof CategoriasEntity) {
//            return ((CategoriasEntity) o).getIdCategoria().toString();
//        }
//
//        return "";
//    }
//
//    private CategoriasModel lookupCategoriasModelBean() {
//        try {
//            Context c = new InitialContext();
//            return (CategoriasModel) c.lookup("java:global/com_utils_war_1.0-SNAPSHOT/CategoriasModel!com.utils.Model.CategoriasModel");
//        } catch (NamingException ne) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
//            throw new RuntimeException(ne);
//        }
//    }
//
//}
