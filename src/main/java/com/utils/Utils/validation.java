/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 *
 * @author jonat
 */
public class validation {

    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String Code(String nombre, String apellido) {
        String cadena = "";
        String[] name = nombre.split("");
        String[] lastName = apellido.split("");
        cadena = name[0] + lastName[0];
        Random r = new Random();
        StringBuilder companyCodeBuilder = new StringBuilder(cadena);
        for (int i = 0; i < 6; i++) {
            int valorDado = r.nextInt(10);
            companyCodeBuilder.append(valorDado);
        }
        cadena = companyCodeBuilder.toString();
        return cadena.toUpperCase();
    }
    
    public static String CodeList(){
        String cadena = "LIS";
        Random r = new Random();
        StringBuilder companyCodeBuilder = new StringBuilder(cadena);
        for (int i = 0; i < 5; i++) {
            int valorDado = r.nextInt(10);
            companyCodeBuilder.append(valorDado);
        }
        cadena = companyCodeBuilder.toString();
        return cadena.toUpperCase();
    }

}
