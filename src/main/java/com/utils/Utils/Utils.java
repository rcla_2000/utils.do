/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 *
 * @author HP
 */
public class Utils {

    public static String generarCodigo() {
        Random rnd = new Random(System.currentTimeMillis());
        String codigo = "";

        for (int i = 0; i < 8; i++) {
            codigo += rnd.nextInt(10);
        }

        return codigo.trim();
    }

    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    
        //Generar token
    public String generatePassword() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		StringBuilder sb = new StringBuilder(8);
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String pass = sb.toString();
		
		
		return pass;
	}
    
    
    public static String generarCodigoSolicitud() {
        Random rnd = new Random(System.currentTimeMillis());
        String codigo = "SOL";

        for (int i = 0; i < 5; i++) {
            codigo += rnd.nextInt(10);
        }

        return codigo.trim();
    }

}
