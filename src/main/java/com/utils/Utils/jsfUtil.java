/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Utils;

import com.utils.Beans.ImagenBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jonat
 */
public class jsfUtil {

    public static String generarCodigo() {
        Random rnd = new Random(System.currentTimeMillis());
        String codigo = "";
        List<String> partes = new ArrayList<>();
        String letras[] = {"aA", "bB", "cC", "dD", "eE", "fF", "gG", "hH", "iI", "jJ", "kK", "lL", "mM", "nN", "oO", "pP", "qQ", "rR", "sS", "tT", "uU", "vV", "wW", "xX", "yY", "zZ"};

        int azar = 1 + rnd.nextInt((8 + 1) - 1);
        int i = 1;
        while (i <= azar) {
            partes.add(Character.toString(letras[rnd.nextInt(26)].charAt(rnd.nextInt(2))));
            i++;
        }

        while (partes.size() < 8) {
            partes.add(Integer.toString(rnd.nextInt(10)));
        }

        int[] posiciones = numerosAleatorios();

        for (int j = 0; j < partes.size(); j++) {
            codigo += partes.get(posiciones[j]);
        }

        return codigo;
    }

    public static int[] numerosAleatorios() {
        int azar[] = new int[8];
        Random rnd = new Random(System.currentTimeMillis());
        int num;
        boolean b;

        for (int i = 0; i < azar.length; i++) {
            do {
                num = rnd.nextInt(8);
                b = false;
                for (int j = 0; j < i; j++) {
                    if (azar[j] == num) {
                        b = true;
                    }
                }
            } while (b);

            azar[i] = num;

        }
        return azar;
    }

    public static void setFlashMessage(String name, String msg) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put(name, msg);
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
}
