/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "sondeos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SondeosEntity.findAll", query = "SELECT s FROM SondeosEntity s")
    , @NamedQuery(name = "SondeosEntity.findByIdSondeo", query = "SELECT s FROM SondeosEntity s WHERE s.idSondeo = :idSondeo")
    , @NamedQuery(name = "SondeosEntity.findByNombre", query = "SELECT s FROM SondeosEntity s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "SondeosEntity.findByFechaSondeo", query = "SELECT s FROM SondeosEntity s WHERE s.fechaSondeo = :fechaSondeo")})
public class SondeosEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "id_sondeo")
    private String idSondeo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    private String nombre;
    @Column(name = "fecha_sondeo")
    @Temporal(TemporalType.DATE)
    private Date fechaSondeo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSondeo")
    private List<SondeoProductoEntity> sondeoProductoEntityList;

    public SondeosEntity() {
    }

    public SondeosEntity(String idSondeo) {
        this.idSondeo = idSondeo;
    }

    public SondeosEntity(String idSondeo, String nombre) {
        this.idSondeo = idSondeo;
        this.nombre = nombre;
    }

    public String getIdSondeo() {
        return idSondeo;
    }

    public void setIdSondeo(String idSondeo) {
        this.idSondeo = idSondeo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaSondeo() {
        return fechaSondeo;
    }

    public void setFechaSondeo(Date fechaSondeo) {
        this.fechaSondeo = fechaSondeo;
    }

    @XmlTransient
    public List<SondeoProductoEntity> getSondeoProductoEntityList() {
        return sondeoProductoEntityList;
    }

    public void setSondeoProductoEntityList(List<SondeoProductoEntity> sondeoProductoEntityList) {
        this.sondeoProductoEntityList = sondeoProductoEntityList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSondeo != null ? idSondeo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SondeosEntity)) {
            return false;
        }
        SondeosEntity other = (SondeosEntity) object;
        if ((this.idSondeo == null && other.idSondeo != null) || (this.idSondeo != null && !this.idSondeo.equals(other.idSondeo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.SondeosEntity[ idSondeo=" + idSondeo + " ]";
    }
    
}
