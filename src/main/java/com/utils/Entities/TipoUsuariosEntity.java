/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "tipo_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoUsuariosEntity.findAll", query = "SELECT t FROM TipoUsuariosEntity t")
    , @NamedQuery(name = "TipoUsuariosEntity.findById", query = "SELECT t FROM TipoUsuariosEntity t WHERE t.id = :id")
    , @NamedQuery(name = "TipoUsuariosEntity.findByNombre", query = "SELECT t FROM TipoUsuariosEntity t WHERE t.nombre = :nombre")})
public class TipoUsuariosEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoUsuario")
    private List<UsuariosEntity> usuariosEntityList;

    public TipoUsuariosEntity() {
    }

    public TipoUsuariosEntity(Integer id) {
        this.id = id;
    }

    public TipoUsuariosEntity(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<UsuariosEntity> getUsuariosEntityList() {
        return usuariosEntityList;
    }

    public void setUsuariosEntityList(List<UsuariosEntity> usuariosEntityList) {
        this.usuariosEntityList = usuariosEntityList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoUsuariosEntity)) {
            return false;
        }
        TipoUsuariosEntity other = (TipoUsuariosEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.TipoUsuariosEntity[ id=" + id + " ]";
    }
    
}
