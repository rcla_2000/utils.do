/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "productos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductosEntity.findAll", query = "SELECT p FROM ProductosEntity p ORDER BY p.nombre")
    , @NamedQuery(name = "ProductosEntity.findByIdProducto", query = "SELECT p FROM ProductosEntity p WHERE p.idProducto = :idProducto")
    , @NamedQuery(name = "ProductosEntity.findByNombre", query = "SELECT p FROM ProductosEntity p WHERE p.nombre LIKE CONCAT('%', :nombre, '%') ORDER BY p.nombre")
    , @NamedQuery(name = "ProductosEntity.findByMarca", query = "SELECT p FROM ProductosEntity p WHERE p.marca = :marca")
    , @NamedQuery(name = "ProductosEntity.findByStock", query = "SELECT p FROM ProductosEntity p WHERE p.stock = :stock")
    , @NamedQuery(name = "ProductosEntity.findByCategoria", query = "SELECT p FROM ProductosEntity p WHERE p.idCategoria = :idCategoria ORDER BY p.nombre")
})
public class ProductosEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "id_producto")
    private String idProducto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    private int stock;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idProducto")
    private ImagenesProductoEntity imagenesProductoEntity;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProducto")
    private List<ListaProductosEntity> listaProductosEntityList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProducto")
    private List<LibreriaProductoEntity> libreriaProductoEntityList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProducto")
    private List<SondeoProductoEntity> sondeoProductoEntityList;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne(optional = false)
    private CategoriasEntity idCategoria;

    public ProductosEntity() {
        this.idProducto = "";
        this.nombre = "";
        this.marca = "";
        this.stock = 0;
        this.idCategoria = new CategoriasEntity();
    }

    public ProductosEntity(String idProducto) {
        this.idProducto = idProducto;
    }

    public ProductosEntity(String idProducto, String nombre, String marca, int stock) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.marca = marca;
        this.stock = stock;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public ImagenesProductoEntity getImagenesProductoEntity() {
        return imagenesProductoEntity;
    }

    public void setImagenesProductoEntity(ImagenesProductoEntity imagenesProductoEntity) {
        this.imagenesProductoEntity = imagenesProductoEntity;
    }

    @XmlTransient
    public List<ListaProductosEntity> getListaProductosEntityList() {
        return listaProductosEntityList;
    }

    public void setListaProductosEntityList(List<ListaProductosEntity> listaProductosEntityList) {
        this.listaProductosEntityList = listaProductosEntityList;
    }

    @XmlTransient
    public List<LibreriaProductoEntity> getLibreriaProductoEntityList() {
        return libreriaProductoEntityList;
    }

    public void setLibreriaProductoEntityList(List<LibreriaProductoEntity> libreriaProductoEntityList) {
        this.libreriaProductoEntityList = libreriaProductoEntityList;
    }

    @XmlTransient
    public List<SondeoProductoEntity> getSondeoProductoEntityList() {
        return sondeoProductoEntityList;
    }

    public void setSondeoProductoEntityList(List<SondeoProductoEntity> sondeoProductoEntityList) {
        this.sondeoProductoEntityList = sondeoProductoEntityList;
    }

    public CategoriasEntity getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(CategoriasEntity idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductosEntity)) {
            return false;
        }
        ProductosEntity other = (ProductosEntity) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.ProductosEntity[ idProducto=" + idProducto + " ]";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
