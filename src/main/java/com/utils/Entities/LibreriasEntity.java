/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "librerias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LibreriasEntity.findAll", query = "SELECT l FROM LibreriasEntity l")
    , @NamedQuery(name = "LibreriasEntity.findByIdLibreria", query = "SELECT l FROM LibreriasEntity l WHERE l.idLibreria = :idLibreria")
    , @NamedQuery(name = "LibreriasEntity.findByNombre", query = "SELECT l FROM LibreriasEntity l WHERE l.nombre LIKE CONCAT('%', :nombre, '%')")
    , @NamedQuery(name = "LibreriasEntity.findByDireccion", query = "SELECT l FROM LibreriasEntity l WHERE l.direccion = :direccion")
    , @NamedQuery(name = "LibreriasEntity.findByDepartamento", query = "SELECT l FROM LibreriasEntity l WHERE l.departamento = :departamento")
    , @NamedQuery(name = "LibreriasEntity.findByMunicipio", query = "SELECT l FROM LibreriasEntity l WHERE l.municipio = :municipio")
    , @NamedQuery(name = "LibreriasEntity.findByTelefono", query = "SELECT l FROM LibreriasEntity l WHERE l.telefono = :telefono")
    , @NamedQuery(name = "LibreriasEntity.findByEmail", query = "SELECT l FROM LibreriasEntity l WHERE l.email = :email")
    , @NamedQuery(name = "LibreriasEntity.findDepartamentos", query = "SELECT DISTINCT l.departamento FROM LibreriasEntity l")
    , @NamedQuery(name = "LibreriasEntity.findMunicipios", query = "SELECT DISTINCT l.municipio FROM LibreriasEntity l WHERE l.departamento = :departamento")})
public class LibreriasEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "id_libreria")
    private String idLibreria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    private String departamento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    private String municipio;
    @Size(max = 9)
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Correo electrónico no válido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 120)
    private String email;
    @JoinColumn(name = "administrador", referencedColumnName = "id_usuario")
    @ManyToOne
    private UsuariosEntity administrador;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLibreria")
    private List<LibreriaProductoEntity> libreriaProductoEntityList;

    public LibreriasEntity() {
        this.idLibreria = "";
        this.nombre = "";
        this.direccion = "";
        this.departamento = "";
        this.municipio = "";
        this.telefono ="";
        this.email = "";
        this.administrador = new UsuariosEntity();
    }

    public LibreriasEntity(String idLibreria) {
        this.idLibreria = idLibreria;
    }

    public LibreriasEntity(String idLibreria, String nombre, String direccion, String departamento, String municipio) {
        this.idLibreria = idLibreria;
        this.nombre = nombre;
        this.direccion = direccion;
        this.departamento = departamento;
        this.municipio = municipio;
    }

    public String getIdLibreria() {
        return idLibreria;
    }

    public void setIdLibreria(String idLibreria) {
        this.idLibreria = idLibreria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UsuariosEntity getAdministrador() {
        return administrador;
    }

    public void setAdministrador(UsuariosEntity administrador) {
        this.administrador = administrador;
    }

    @XmlTransient
    public List<LibreriaProductoEntity> getLibreriaProductoEntityList() {
        return libreriaProductoEntityList;
    }

    public void setLibreriaProductoEntityList(List<LibreriaProductoEntity> libreriaProductoEntityList) {
        this.libreriaProductoEntityList = libreriaProductoEntityList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLibreria != null ? idLibreria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LibreriasEntity)) {
            return false;
        }
        LibreriasEntity other = (LibreriasEntity) object;
        if ((this.idLibreria == null && other.idLibreria != null) || (this.idLibreria != null && !this.idLibreria.equals(other.idLibreria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.LibreriasEntity[ idLibreria=" + idLibreria + " ]";
    }

}
