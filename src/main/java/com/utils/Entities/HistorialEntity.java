/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "historial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialEntity.findAll", query = "SELECT h FROM HistorialEntity h")
    , @NamedQuery(name = "HistorialEntity.findByIdHistorial", query = "SELECT h FROM HistorialEntity h WHERE h.idHistorial = :idHistorial")
    , @NamedQuery(name = "HistorialEntity.findByBusqueda", query = "SELECT h FROM HistorialEntity h WHERE h.busqueda LIKE CONCAT('%',:busqueda,'%')  and h.idUsuario = :idUsuario")
    , @NamedQuery(name = "HistorialEntity.findByFechaHora", query = "SELECT h FROM HistorialEntity h WHERE h.fechaHora LIKE CONCAT('%',:fechaHora,'%') and h.idUsuario = :idUsuario")
    , @NamedQuery(name = "HistorialEntity.findByIdUsuario", query = "SELECT h FROM HistorialEntity h WHERE h.idUsuario = :idUsuario")
})
public class HistorialEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_historial")
    private Integer idHistorial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    private String busqueda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private UsuariosEntity idUsuario;

    public HistorialEntity() {
        busqueda = "";
        fechaHora = null;
        idUsuario = new UsuariosEntity();
    }

    public HistorialEntity(Integer idHistorial) {
        this.idHistorial = idHistorial;
    }

    public HistorialEntity(Integer idHistorial, String busqueda, Date fechaHora) {
        this.idHistorial = idHistorial;
        this.busqueda = busqueda;
        this.fechaHora = fechaHora;
    }

    public Integer getIdHistorial() {
        return idHistorial;
    }

    public void setIdHistorial(Integer idHistorial) {
        this.idHistorial = idHistorial;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public UsuariosEntity getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UsuariosEntity idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHistorial != null ? idHistorial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialEntity)) {
            return false;
        }
        HistorialEntity other = (HistorialEntity) object;
        if ((this.idHistorial == null && other.idHistorial != null) || (this.idHistorial != null && !this.idHistorial.equals(other.idHistorial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.HistorialEntity[ idHistorial=" + idHistorial + " ]";
    }

}
