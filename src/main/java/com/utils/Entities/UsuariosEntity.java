/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuariosEntity.findAll", query = "SELECT u FROM UsuariosEntity u")
    , @NamedQuery(name = "UsuariosEntity.findByIdUsuario", query = "SELECT u FROM UsuariosEntity u WHERE u.idUsuario = :idUsuario")
    , @NamedQuery(name = "UsuariosEntity.findByNombres", query = "SELECT u FROM UsuariosEntity u WHERE u.nombres = :nombres")
    , @NamedQuery(name = "UsuariosEntity.findByApellidos", query = "SELECT u FROM UsuariosEntity u WHERE u.apellidos = :apellidos")
    , @NamedQuery(name = "UsuariosEntity.findBySexo", query = "SELECT u FROM UsuariosEntity u WHERE u.sexo = :sexo")
    , @NamedQuery(name = "UsuariosEntity.findByFechaNacimiento", query = "SELECT u FROM UsuariosEntity u WHERE u.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "UsuariosEntity.findByEmail", query = "SELECT u FROM UsuariosEntity u WHERE u.email = :email")
    , @NamedQuery(name = "UsuariosEntity.findByDui", query = "SELECT u FROM UsuariosEntity u WHERE u.dui = :dui")
    , @NamedQuery(name = "UsuariosEntity.findByDireccion", query = "SELECT u FROM UsuariosEntity u WHERE u.direccion = :direccion")
    , @NamedQuery(name = "UsuariosEntity.findByTelefono", query = "SELECT u FROM UsuariosEntity u WHERE u.telefono = :telefono")
    , @NamedQuery(name = "UsuariosEntity.findByPassword", query = "SELECT u FROM UsuariosEntity u WHERE u.password = :password")
    , @NamedQuery(name = "UsuariosEntity.findByTokenPassword", query = "SELECT u FROM UsuariosEntity u WHERE u.tokenPassword = :tokenPassword")
    , @NamedQuery(name = "UsuariosEntity.findByNSesiones", query = "SELECT u FROM UsuariosEntity u WHERE u.nSesiones = :nSesiones")
    , @NamedQuery(name = "UsuariosEntity.findByUltimoAcceso", query = "SELECT u FROM UsuariosEntity u WHERE u.ultimoAcceso = :ultimoAcceso")})
public class UsuariosEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "id_usuario")
    private String idUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 130)
    private String nombres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 130)
    private String apellidos;
    @Basic(optional = false)
    @NotNull
    private Character sexo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Correo electrónico no válido")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    private String email;
    @Size(max = 10)
    private String dui;
    @Size(max = 300)
    private String direccion;
    @Size(max = 9)
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String password;
    @Size(max = 100)
    @Column(name = "token_password")
    private String tokenPassword;
    @Column(name = "n_sesiones")
    private Integer nSesiones;
    @Column(name = "ultimo_acceso")
    @Temporal(TemporalType.DATE)
    private Date ultimoAcceso;
    @OneToOne(mappedBy = "administrador")
    private LibreriasEntity libreriasEntity;
    @JoinColumn(name = "tipo_usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoUsuariosEntity tipoUsuario;
    @OneToMany(mappedBy = "usuario")
    private List<SolicitudesEntity> solicitudesEntityList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuario")
    private List<HistorialEntity> historialEntityList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuario")
    private List<ListasEntity> listasEntityList;

    public UsuariosEntity() {
        this.idUsuario = "";
        this.nombres = "";
        this.apellidos = "";
        this.sexo = ' ';
        this.dui = "";
        this.email = "";
        this.direccion = "";
        this.telefono = "";
        this.password = "";
        this.tipoUsuario = new TipoUsuariosEntity();
    }

    public UsuariosEntity(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public UsuariosEntity(String idUsuario, String nombres, String apellidos, Character sexo, Date fechaNacimiento, String email, String password) {
        this.idUsuario = idUsuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.password = password;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTokenPassword() {
        return tokenPassword;
    }

    public void setTokenPassword(String tokenPassword) {
        this.tokenPassword = tokenPassword;
    }

    public Integer getNSesiones() {
        return nSesiones;
    }

    public void setNSesiones(Integer nSesiones) {
        this.nSesiones = nSesiones;
    }

    public Date getUltimoAcceso() {
        return ultimoAcceso;
    }

    public void setUltimoAcceso(Date ultimoAcceso) {
        this.ultimoAcceso = ultimoAcceso;
    }

    public TipoUsuariosEntity getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuariosEntity tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @XmlTransient
    public List<SolicitudesEntity> getSolicitudesEntityList() {
        return solicitudesEntityList;
    }

    public void setSolicitudesEntityList(List<SolicitudesEntity> solicitudesEntityList) {
        this.solicitudesEntityList = solicitudesEntityList;
    }

    @XmlTransient
    public List<HistorialEntity> getHistorialEntityList() {
        return historialEntityList;
    }

    public void setHistorialEntityList(List<HistorialEntity> historialEntityList) {
        this.historialEntityList = historialEntityList;
    }

    @XmlTransient
    public List<ListasEntity> getListasEntityList() {
        return listasEntityList;
    }

    public void setListasEntityList(List<ListasEntity> listasEntityList) {
        this.listasEntityList = listasEntityList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosEntity)) {
            return false;
        }
        UsuariosEntity other = (UsuariosEntity) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.UsuariosEntity[ idUsuario=" + idUsuario + " ]";
    }

    public LibreriasEntity getLibreriasEntity() {
        return libreriasEntity;
    }

    public void setLibreriasEntity(LibreriasEntity libreriasEntity) {
        this.libreriasEntity = libreriasEntity;
    }
    
}
