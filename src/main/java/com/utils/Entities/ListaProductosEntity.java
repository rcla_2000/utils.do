/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "lista_productos")
@XmlRootElement
@NamedQueries({@NamedQuery(name = "ListaProductosEntity.findById", query = "SELECT l FROM ListaProductosEntity l WHERE l.id = :id")
    , @NamedQuery(name = "ListaProductosEntity.findByCantidad", query = "SELECT l FROM ListaProductosEntity l WHERE l.cantidad = :cantidad")})
public class ListaProductosEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    private int cantidad;
    @JoinColumn(name = "id_lista", referencedColumnName = "id_lista")
    @ManyToOne(optional = false)
    private ListasEntity idLista;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private ProductosEntity idProducto;

    public ListaProductosEntity() {
    }

    public ListaProductosEntity(Integer id) {
        this.id = id;
    }

    public ListaProductosEntity(Integer id, int cantidad) {
        this.id = id;
        this.cantidad = cantidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public ListasEntity getIdLista() {
        return idLista;
    }

    public void setIdLista(ListasEntity idLista) {
        this.idLista = idLista;
    }

    public ProductosEntity getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(ProductosEntity idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaProductosEntity)) {
            return false;
        }
        ListaProductosEntity other = (ListaProductosEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.ListaProductosEntity[ id=" + id + " ]";
    }
    
}
