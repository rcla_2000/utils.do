/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "listas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListasEntity.findAll", query = "SELECT l FROM ListasEntity l")
    , @NamedQuery(name = "ListasEntity.findByIdUsuario", query = "SELECT l FROM ListasEntity l WHERE l.idUsuario = :codigo")
    , @NamedQuery(name = "ListasEntity.findByNombre", query = "SELECT l FROM ListasEntity l WHERE l.nombre = :nombre")})
public class ListasEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "id_lista")
    private String idLista;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLista")
    private List<ListaProductosEntity> listaProductosEntityList;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private UsuariosEntity idUsuario;

    public ListasEntity() {
    }

    public ListasEntity(String idLista) {
        this.idLista = idLista;
    }

    public ListasEntity(String idLista, String nombre) {
        this.idLista = idLista;
        this.nombre = nombre;
    }

    public String getIdLista() {
        return idLista;
    }

    public void setIdLista(String idLista) {
        this.idLista = idLista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<ListaProductosEntity> getListaProductosEntityList() {
        return listaProductosEntityList;
    }

    public void setListaProductosEntityList(List<ListaProductosEntity> listaProductosEntityList) {
        this.listaProductosEntityList = listaProductosEntityList;
    }

    public UsuariosEntity getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UsuariosEntity idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLista != null ? idLista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListasEntity)) {
            return false;
        }
        ListasEntity other = (ListasEntity) object;
        if ((this.idLista == null && other.idLista != null) || (this.idLista != null && !this.idLista.equals(other.idLista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.ListasEntity[ idLista=" + idLista + " ]";
    }
    
}
