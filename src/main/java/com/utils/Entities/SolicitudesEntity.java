/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "solicitudes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudesEntity.findAll", query = "SELECT s FROM SolicitudesEntity s")
    , @NamedQuery(name = "SolicitudesEntity.findByIdSolicitud", query = "SELECT s FROM SolicitudesEntity s WHERE s.idSolicitud = :idSolicitud")
    , @NamedQuery(name = "SolicitudesEntity.findByNombres", query = "SELECT s FROM SolicitudesEntity s WHERE s.nombres = :nombres")
    , @NamedQuery(name = "SolicitudesEntity.findByApellidos", query = "SELECT s FROM SolicitudesEntity s WHERE s.apellidos = :apellidos")
    , @NamedQuery(name = "SolicitudesEntity.findByDui", query = "SELECT s FROM SolicitudesEntity s WHERE s.dui = :dui")
    , @NamedQuery(name = "SolicitudesEntity.findByDireccion", query = "SELECT s FROM SolicitudesEntity s WHERE s.direccion = :direccion")
    , @NamedQuery(name = "SolicitudesEntity.findByTelefono", query = "SELECT s FROM SolicitudesEntity s WHERE s.telefono = :telefono")
    , @NamedQuery(name = "SolicitudesEntity.findByEmail", query = "SELECT s FROM SolicitudesEntity s WHERE s.email = :email")
    , @NamedQuery(name = "SolicitudesEntity.findByLibreria", query = "SELECT s FROM SolicitudesEntity s WHERE s.libreria = :libreria")
    , @NamedQuery(name = "SolicitudesEntity.findByDireccionLibreria", query = "SELECT s FROM SolicitudesEntity s WHERE s.direccionLibreria = :direccionLibreria")
    , @NamedQuery(name = "SolicitudesEntity.findByDepartamento", query = "SELECT s FROM SolicitudesEntity s WHERE s.departamento = :departamento")
    , @NamedQuery(name = "SolicitudesEntity.findByMunicipio", query = "SELECT s FROM SolicitudesEntity s WHERE s.municipio = :municipio")
    , @NamedQuery(name = "SolicitudesEntity.findByEstado", query = "SELECT s FROM SolicitudesEntity s WHERE s.estado = :estado")})
public class SolicitudesEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "id_solicitud")
    private String idSolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 130)
    private String nombres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 130)
    private String apellidos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    private String dui;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Correo electrónico no válido")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    private String libreria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "direccion_libreria")
    private String direccionLibreria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String departamento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    private String municipio;
    @Basic(optional = false)
    @NotNull
    private int estado;
    @JoinColumn(name = "usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private UsuariosEntity usuario;

    public SolicitudesEntity() {
    }

    public SolicitudesEntity(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public SolicitudesEntity(String idSolicitud, String nombres, String apellidos, String dui, String direccion, String telefono, String email, String libreria, String direccionLibreria, String departamento, String municipio, int estado) {
        this.idSolicitud = idSolicitud;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.dui = dui;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.libreria = libreria;
        this.direccionLibreria = direccionLibreria;
        this.departamento = departamento;
        this.municipio = municipio;
        this.estado = estado;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLibreria() {
        return libreria;
    }

    public void setLibreria(String libreria) {
        this.libreria = libreria;
    }

    public String getDireccionLibreria() {
        return direccionLibreria;
    }

    public void setDireccionLibreria(String direccionLibreria) {
        this.direccionLibreria = direccionLibreria;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public UsuariosEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuariosEntity usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSolicitud != null ? idSolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudesEntity)) {
            return false;
        }
        SolicitudesEntity other = (SolicitudesEntity) object;
        if ((this.idSolicitud == null && other.idSolicitud != null) || (this.idSolicitud != null && !this.idSolicitud.equals(other.idSolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.SolicitudesEntity[ idSolicitud=" + idSolicitud + " ]";
    }
    
}
