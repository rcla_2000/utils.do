/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "libreria_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LibreriaProductoEntity.findAll", query = "SELECT l FROM LibreriaProductoEntity l")
    , @NamedQuery(name = "LibreriaProductoEntity.findById", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.id = :id")
    , @NamedQuery(name = "LibreriaProductoEntity.findByPrecio", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.precio = :precio")
    , @NamedQuery(name = "LibreriaProductoEntity.findByProducto", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.idProducto = :producto ORDER BY l.precio")
    , @NamedQuery(name = "LibreriaProductoEntity.findByDepartamento", query = "SELECT l FROM LibreriaProductoEntity l WHERE  l.idProducto = :producto AND l.idLibreria.departamento = :departamento ORDER BY l.precio")
    , @NamedQuery(name = "LibreriaProductoEntity.findByMunicipio", query = "SELECT l FROM LibreriaProductoEntity l WHERE  l.idProducto = :producto AND l.idLibreria.municipio = :municipio ORDER BY l.precio")
    , @NamedQuery(name = "LibreriaProductoEntity.findByMinPrecio", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.idProducto = :producto AND l.precio = (SELECT MIN(l.precio) from LibreriaProductoEntity l WHERE l.idProducto = :producto)")
    , @NamedQuery(name = "LibreriaProductoEntity.findByMaxPrecio", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.idProducto = :producto AND l.precio = (SELECT MAX(l.precio) from LibreriaProductoEntity l WHERE l.idProducto = :producto)")
    , @NamedQuery(name = "LibreriaProductoEntity.comparacionPrecio", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.idProducto = :producto AND l.idLibreria IN :librerias ORDER BY l.precio")
    , @NamedQuery(name = "LibreriaProductoEntity.productosPorAdministrador", query = "SELECT l FROM LibreriaProductoEntity l WHERE l.idLibreria.administrador = :administrador ORDER BY l.idProducto.nombre")
})
public class LibreriaProductoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    private BigDecimal precio;
    @JoinColumn(name = "id_libreria", referencedColumnName = "id_libreria")
    @ManyToOne(optional = false)
    private LibreriasEntity idLibreria;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private ProductosEntity idProducto;

    public LibreriaProductoEntity() {
        this.idLibreria = new LibreriasEntity();
        this.idProducto = new ProductosEntity();
    }

    public LibreriaProductoEntity(Integer id) {
        this.id = id;
    }

    public LibreriaProductoEntity(Integer id, BigDecimal precio) {
        this.id = id;
        this.precio = precio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public LibreriasEntity getIdLibreria() {
        return idLibreria;
    }

    public void setIdLibreria(LibreriasEntity idLibreria) {
        this.idLibreria = idLibreria;
    }

    public ProductosEntity getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(ProductosEntity idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LibreriaProductoEntity)) {
            return false;
        }
        LibreriaProductoEntity other = (LibreriaProductoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.LibreriaProductoEntity[ id=" + id + " ]";
    }

}
