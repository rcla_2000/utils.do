/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "imagenes_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImagenesProductoEntity.findAll", query = "SELECT i FROM ImagenesProductoEntity i")
    , @NamedQuery(name = "ImagenesProductoEntity.findByIdImagen", query = "SELECT i FROM ImagenesProductoEntity i WHERE i.idImagen = :idImagen")
    , @NamedQuery(name = "ImagenesProductoEntity.findByProducto", query = "SELECT i FROM ImagenesProductoEntity i WHERE i.idProducto = :producto")})
public class ImagenesProductoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_imagen")
    private Integer idImagen;
    @Basic(optional = false)
    @NotNull
    @Lob
    private byte[] imagen;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @OneToOne(optional = false)
    private ProductosEntity idProducto;

    public ImagenesProductoEntity() {
        this.idProducto = new ProductosEntity();
    }

    public ImagenesProductoEntity(Integer idImagen) {
        this.idImagen = idImagen;
    }

    public ImagenesProductoEntity(Integer idImagen, byte[] imagen) {
        this.idImagen = idImagen;
        this.imagen = imagen;
    }

    public Integer getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(Integer idImagen) {
        this.idImagen = idImagen;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public ProductosEntity getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(ProductosEntity idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idImagen != null ? idImagen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenesProductoEntity)) {
            return false;
        }
        ImagenesProductoEntity other = (ImagenesProductoEntity) object;
        if ((this.idImagen == null && other.idImagen != null) || (this.idImagen != null && !this.idImagen.equals(other.idImagen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.utils.Entities.ImagenesProductoEntity[ idImagen=" + idImagen + " ]";
    }

}
